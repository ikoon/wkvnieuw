/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($, Drupal) {

  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.slides = {
    attach: function (context, settings) {
      $(context).find('.field-name-field-slides.swiper-container').once('ifLatestSlider').each(function (){
        var slides = new Swiper ('.field-name-field-slides.swiper-container', {
          autoplay: true,
          loop: true,
          effect: 'fade',
          pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true,
          },
        });
      });
    }
  };

  /**
   * Events on the right off canvas container.
   */
  Drupal.behaviors.rightOffCanvasBehaviour = {
    attach: function (context, settings) {
      // Apply is-active class to menu toggle button.
      $('#right-off-canvas-menu')
        .on('opened.zf.offcanvas', function (e) {
          $('button[data-toggle="right-off-canvas-menu"]').addClass('is-active');
        })
        .on('closed.zf.offcanvas', function (e) {
          $('button[data-toggle="right-off-canvas-menu"]').removeClass('is-active');
        })
      ;

    }
  };

  /*
   * Provides increment and decrement icons on product quantity field.
   */
  Drupal.behaviors.productQuantityIncrement = {
    attach: function (context, settings) {
      if (typeof Drupal.behaviors.mviCustomUnitsQuantityField === 'undefined') {
        // Shopping cart form
        // This is not needed when commerce_weight module is enabled.
        $(".views-field-edit-quantity").once('productQuantityDecrement').prepend('<div class="dec qty-button">-</div>');
        $(".views-field-edit-quantity").once('productQuantityIncrement').append('<div class="inc qty-button">+</div>');

        // Product add to cart form
        // This is not needed when commerce_weight module is enabled.
        $(".commerce-order-item-add-to-cart-form .field--name-quantity, .add-menu-to-cart-form .js-form-item-quantity")
          .once('productQuantityDecrement').prepend('<div class="dec qty-button">-</div>');
        $(".commerce-order-item-add-to-cart-form .field--name-quantity, .add-menu-to-cart-form .js-form-item-quantity")
          .once('productQuantityIncrement').append('<div class="inc qty-button">+</div>');

        $(".add-menu-to-cart-form .js-form-item-quantity label, .add-menu-to-cart-form .js-form-item-quantity input")
          .wrapAll('<div class="add-menu-to-cart-form__lbl--input" />');

        $(".qty-button").once().click(function () {
          var $button = $(this);
          var oldValue = $button.parent().find("input").val();
          var step = $button.parent().find("input").attr('step');
          var min = $button.parent().find("input").attr('min');

          if ($button.text() == "+") {
            var newVal = parseFloat(oldValue) + parseFloat(step);
          } else {
            // Don't allow decrementing below zero
            if (oldValue > 0 && oldValue > min) {
              var newVal = parseFloat(oldValue) - parseFloat(step);
            } else if(min) {
              newVal = min;
            } else {
              newVal = 0;
            }
          }
          $button.parent().find("input").val(newVal);

        });
      }
    }
  };

  /*
   * Behavior for the productfilters on mobile devices.
   */
  Drupal.behaviors.productsFilterControls = {
    attach: function (context, settings) {
      $('.close-filters').click(function(){
        if($(window).width() >= 900){
          $('.filter-region-left').addClass('closed');
          $('.main-region').addClass('filters-closed');

          setTimeout(function(){
          }, 200);
        } else {
          $('.filter-region-left').removeClass('open');
          $('.main-region').addClass('filters-closed');
          $('.main-region').removeClass('filters-open');
        }
      });

      $('.open-filters').click(function(){
        if($(window).width() >= 900) {
          $('.main-region').removeClass('filters-closed');
          $('.filter-region-left').removeClass('closed');
        } else {
          $('.main-region').removeClass('filters-closed');
          $('.main-region').addClass('filters-open');
          $('.filter-region-left').addClass('open');
        }
      });
    }
  };

  /**
   * Close the add to cart pop-up
   */
  Drupal.behaviors.addToCartPopUp = {
    attach: function (context, settings) {
      //.add-to-cart-message-actions .ui-dialog-titlebar-close
      $('.close-dialog-button').once('auto-close-modal').on('click', function() {
        $('.ui-dialog-titlebar .ui-dialog-titlebar-close').click();
      });
    }
  };

  Drupal.behaviors.productDetailSlider = {
    attach: function (context, settings) {
      $(context).find('.product-detail .product-slider').once('productDetailSlider').each(function (){
        var productSlider = new Swiper('.product-detail .product-slider', {
          loop: false,
          slidesPerView: 1,
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          }
        })
      });
    }
  };

  Drupal.behaviors.relatedProductsSlider = {
    attach: function (context, settings) {
      $(context).find('.related-products-slider').once('relatedProductsSlider').each(function (){
        var relatedProductSlider = new Swiper('.related-products-slider', {
          loop: true,
          slidesPerView: 3,
          spaceBetween: 28,
          navigation: {
            nextEl: '.product-button-next',
            prevEl: '.product-button-prev',
          },
          breakpoints: {
            1200: {
              slidesPerView: 3,
            }
          }
        })
      });
    }
  };

  Drupal.behaviors.customCheckout = {
    attach: function (context, settings) {
      setPickupDeliveryVisibility();

      // When shipping method "Pickup" is clicked.
      $('input[value="2--default"]', context).click(function () {
        setPickupDeliveryVisibility();
      });
      // When shipping method "Pickup" is clicked.
      $('input[value="1--default"]', context).click(function () {
        setPickupDeliveryVisibility();
      });

      function setPickupDeliveryVisibility() {
        if($('input[value="2--default"]').is(':checked')){
          $('.checkout-pane-preferred-delivery').addClass('hidden');
          $('.checkout-pane-pickup-order').removeClass('hidden');

        } else {
          $('.checkout-pane-preferred-delivery').removeClass('hidden');
          $('.checkout-pane-pickup-order').addClass('hidden');
        }
      }
    }
  };

  Drupal.behaviors.paragraphLightGallery= {
    attach: function (context, settings) {
      $('.paragraph-gallery').lightGallery();
    }
  };

  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.cookieSettings = {
    attach: function (context, settings) {
      $('span[data-action="cookie_settings"]', context).click(function() {
        $('.eu-cookie-withdraw-tab').click();
      });
      $(function(){
        $('#sliding-popup').css({bottom: - $('#sliding-popup').height()});
      });
    }
  };


  Drupal.behaviors.scrollTo = {
    attach: function (context, settings) {
      $('.scroll-icon').on('click', function(){
        $('html,body').animate({
            scrollTop: $('article.home_page .view-mode-full').offset().top},
          'slow');
      });
    }
  };

})(jQuery, Drupal);
