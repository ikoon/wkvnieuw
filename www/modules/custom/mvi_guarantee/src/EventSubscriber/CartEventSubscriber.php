<?php

namespace Drupal\mvi_guarantee\EventSubscriber;

use Drupal\commerce_cart\Event\CartEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;

use Drupal\mvi_guarantee\OrderResolver;

/**
 * Cart Event Subscriber.
 */
class CartEventSubscriber implements EventSubscriberInterface {

  /**
   * The cart manager.
   *
   * @var \Drupal\mvi_guarantee\OrderResolver
   */
  protected $orderResolver;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\mvi_guarantee\OrderResolver $order_resolver
   *   The messenger.
   */
  public function __construct(OrderResolver $order_resolver) {
    $this->orderResolver = $order_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      CartEvents::CART_ENTITY_ADD => [
        ['setGuaranteeAdjustment']
      ],
      CartEvents::CART_ORDER_ITEM_UPDATE => [
        ['setGuaranteeAdjustment']
      ],
      CartEvents::CART_ORDER_ITEM_REMOVE => [
        ['setGuaranteeAdjustment']
      ],

      // debug, uncomment this and the debug function and be sure to set a valid order id in the debug functoin:
      // 'kernel.request' => [
      //   ['debug']
      // ]
    ];
  }

  /**
   * Adds / removes / edits 
   *
   * @param \Drupal\commerce_cart\Event\Event $event
   *   The cart add event.
   *
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
  public function setGuaranteeAdjustment(Event $event) {
    $this->orderResolver->setGuaranteeAdjustment($event->getCart());
  }

  // public function debug() {
  //   $this->orderResolver->setGuaranteeAdjustment(\Drupal\commerce_order\Entity\Order::load(25));
  // }
}