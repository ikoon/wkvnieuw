<?php

namespace Drupal\mvi_guarantee\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class MviGuaranteeConfigForm.
 */
class MviGuaranteeConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'mvi_guarantee.mviguaranteeconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mvi_guarantee_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('mvi_guarantee.mviguaranteeconfig');
    $form['max_combined_guarantee_price'] = [
      '#type' => 'number',
      '#title' => $this->t('Max combined guarantee price'),
      '#description' => $this->t('The maximum price, per order, for the guarantee'),
      '#default_value' => $config->get('max_combined_guarantee_price'),
      '#required' => true
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('mvi_guarantee.mviguaranteeconfig')
      ->set('max_combined_guarantee_price', $form_state->getValue('max_combined_guarantee_price'))
      ->save();
  }

}
