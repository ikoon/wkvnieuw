<?php

namespace Drupal\mvi_guarantee\Plugin\views\field;

use Drupal\commerce_order\Entity\OrderItem;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_price\CurrencyFormatter;

use Drupal\mvi_guarantee\OrderResolver;

/**
 * Field that shows the total warranty amount for the given order_item.
 *
 * @ViewsField("mvi_guarantee_order_item_warranty")
 */
class OrderItemWarranty extends FieldPluginBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\mvi_guarantee\OrderResolver
   */
  protected $orderResolver;

  /**
   * @var \Drupal\commerce_price\CurrencyFormatter
   */
  protected $currencyFormatter;

  /**
   * Constructs a new WarrantySummary object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\mvi_guarantee\OrderResolver $order_resolver
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, OrderResolver $order_resolver, CurrencyFormatter $currency_formatter) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->orderResolver = $order_resolver;
    $this->currencyFormatter = $currency_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('mvi_guarantee.order_resolver'),
      $container->get('commerce_price.currency_formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    return parent::defineOptions();
  }

  /**
   * {@inheritdoc}
   */
  public function clickSortable() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing.
  }

  public function render(ResultRow $values) {
    /** @var \Drupal\commerce_order\Entity\Order $order */
    $order = $values->_entity;

    /** @var \Drupal\commerce_order\Entity\OrderItem $orderItem */
    $orderItem = $order->getItems()[$values->index];

    if(!$orderItem) {
      return null;
    }

    $warrantyItems = $this->orderResolver->getWarrantyItem($orderItem);

    if (count($warrantyItems) <= 0) {
      return null;
    }

    $markup = '';

    foreach ($warrantyItems as $arr) {
      $price = $arr['price']->multiply($arr['count']);
      $price = $this->currencyFormatter->format($price->getNumber(), $price->getCurrencyCode());
      $markup .= '<div class="order-item--waranty-total">' . $this->t('Warranty: @price', [
        '@price' => $price,
      ]) . '</div>';
    }

    return [
      '#markup' => $markup
    ];
  }
}
