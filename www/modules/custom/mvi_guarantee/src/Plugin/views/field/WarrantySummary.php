<?php

namespace Drupal\mvi_guarantee\Plugin\views\field;

use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_price\CurrencyFormatter;

use Drupal\mvi_guarantee\OrderResolver;

/**
 * Defines a summary of the Warranty Items in this order, shows
 * the Warranty Items and total amount for a single order
 *
 * @ViewsField("mvi_guarantee_warranty_summary")
 */
class WarrantySummary extends FieldPluginBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\mvi_guarantee\OrderResolver
   */
  protected $orderResolver;

  /**
   * @var \Drupal\commerce_price\CurrencyFormatter
   */
  protected $currencyFormatter;

  /**
   * Constructs a new WarrantySummary object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\mvi_guarantee\OrderResolver $order_resolver
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, OrderResolver $order_resolver, CurrencyFormatter $currency_formatter) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->orderResolver = $order_resolver;
    $this->currencyFormatter = $currency_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('mvi_guarantee.order_resolver'),
      $container->get('commerce_price.currency_formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    return parent::defineOptions();
  }

  /**
   * {@inheritdoc}
   */
  public function clickSortable() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing.
  }

  public function render(ResultRow $values) {
    /** @var \Drupal\commerce_order\Entity\Order */
    $order = $values->_entity;
    $warrantyItems = $this->orderResolver->getWarrantyItems($order);
    if (count($warrantyItems) <= 0) {
      return [
        '#markup' => $this->t('None')
      ];
    }

    $markup = '<ul class="warranty_summary">';
    foreach ($warrantyItems as $arr) {
      $title = $arr['warrenty_item']->label();
      $count = $arr['count'];
      $price = $this->currencyFormatter->format($arr['price']->getNumber(), $arr['price']->getCurrencyCode());
      $markup .= '<li>' . $this->t('@title (@price) x@count', [
        '@title' => $title,
        '@count' => $count,
        '@price' => $price,
      ]) . '</li>';
    }
    /** @var \Drupal\commerce_price\Price */
    $total = $this->orderResolver->getTotalWarrantyAmount($order);
    $price = $this->currencyFormatter->format($total->getNumber(), $total->getCurrencyCode());
    $markup .= '<li><b>' . $this->t('Total price: @price', ['@price' => $price]) . '</b></li>';
    $markup .= '</ul>';

    return [
      '#markup' => $markup
    ];
  }
}
