<?php

namespace Drupal\mvi_guarantee\Plugin\views\area;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\area\AreaPluginBase;
use Drupal\views\Plugin\views\argument\NumericArgument;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\mvi_guarantee\OrderResolver;

/**
 * Defines a summary of the Warranty Items in this order, shows
 * the Warranty Items and total amount for a single order
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("mvi_guarantee_commerce_order_warranty_summary")
 */
class WarrantySummary extends AreaPluginBase {

  /**
   * The order storage.
   *
   * @var \Drupal\Core\Entity\Sql\SqlContentEntityStorage
   */
  protected $orderStorage;

  /**
   * @var \Drupal\mvi_guarantee\OrderResolver
   */
  protected $orderResolver;

  /**
   * Constructs a new WarrantySummary instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, OrderResolver $order_resolver) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->orderStorage = $entity_type_manager->getStorage('commerce_order');

    $this->orderResolver = $order_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('mvi_guarantee.order_resolver')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    // $form['empty']['#description'] = $this->t("Even if selected, this area handler will never render if a valid order cannot be found in the View's arguments.");
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    if (!$empty || !empty($this->options['empty'])) {
      foreach ($this->view->argument as $name => $argument) {
        // First look for an order_id argument.
        if (!$argument instanceof NumericArgument) {
          continue;
        }
        if (!in_array($argument->getField(), ['commerce_order.order_id', 'commerce_order_item.order_id'])) {
          continue;
        }
        /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
        if ($order = $this->orderStorage->load($argument->getValue())) {
          return [
            '#markup' => 'test warranty summary content',
          ];
          // $order_total = $order->get('total_price')->view([
          //   'label' => 'hidden',
          //   'type' => 'commerce_order_total_summary',
          //   'weight' => $this->position,
          // ]);
          // $order_total['#prefix'] = '<div data-drupal-selector="order-total-summary">';
          // $order_total['#suffix'] = '</div>';
          // return $order_total;
        }
      }
    }
    return [];
  }

}
