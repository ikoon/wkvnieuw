<?php

namespace Drupal\mvi_guarantee\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\mvi_guarantee\OrderResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\commerce_order\Entity\Order;

use Exception;

/**
 * Class ExportLabelsController.
 */
class ExportLabelsController extends ControllerBase {

  /**
   * @var \Drupal\mvi_guarantee\OrderResolver $orderResolver
   */
  protected $orderResolver;

  /**
   * Class constructor.
   * 
   */
  public function __construct(OrderResolver $order_resolver) {
    $this->orderResolver = $order_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('mvi_guarantee.order_resolver')
    );
  }

  /**
   * Page.
   *
   * @return array
   */
  public function page() {
    $from = \Drupal::request()->query->get('from');
    $to = \Drupal::request()->query->get('to');
    $states = \Drupal::request()->query->get('states');
    if (empty($from) || empty($to) || empty($states)) {
      throw new Exception('Invalid query params');
    }
    
    $fromTimestamp = DrupalDateTime::createFromTimestamp($from);
    $toTimestamp = DrupalDateTime::createFromTimestamp($to);

    $query = \Drupal::entityQuery('commerce_order')
              ->condition('state', explode(';;', $states), 'in')
              ->condition('field_pickup_from', $from, '>=')
              ->condition('field_pickup_from', $to, '<=');
    $entity_ids = $query->execute();
    $orders = Order::loadMultiple($entity_ids);
    $items = [];
    $columns = [t('Order number'), t('Product'), t('Warranty Item'), t('Warranty item index')];
    foreach ($orders as $order) {
      $orderLabels = $this->orderResolver->getLabelsForExport($order);
      foreach ($orderLabels as $label) {
        $items[] = $label;
      }
    }

    return [
      '#theme' => 'mvi_guarantee_export_labels_print',
      '#items' => $items,
      '#from' => $fromTimestamp->format('d/m/Y H:i'),
      '#to' => $toTimestamp->format('d/m/Y H:i'),
      '#columns' => $columns,
      '#attached' => [
        'library' => [
          'mvi_guarantee/mvi_guarantee_export_labels_print',
          'mvi_guarantee/mvi_guarantee_print',
        ]
      ]
    ];
  }

}
