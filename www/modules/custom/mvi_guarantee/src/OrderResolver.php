<?php

namespace Drupal\mvi_guarantee;

use Drupal\commerce_cart\CartManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Config\ConfigFactory;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Adjustment;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductVariation;
// use Drupal\node\Entity\Node;

/**
 * Class OrderResolver.
 */
class OrderResolver
{

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The cart manager.
   *
   * @var \Drupal\commerce_cart\CartManagerInterface
   */
  protected $cartManager;

  /**
   * The entity type manager
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The config factory
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The node storage
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * The taxonomy_term storage
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $taxonomyTermStorage;

  /**
   * Constructs a new OrderResolver object.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\commerce_cart\CartManagerInterface $cart_manager
   *   The messenger.
   */
  public function __construct(MessengerInterface $messenger, CartManagerInterface $cart_manager, EntityTypeManager $entity_type_manager, ConfigFactory $config_factory)
  {
    $this->messenger = $messenger; // @TODO, clean up if never used
    $this->cartManager = $cart_manager; // @TODO, clean up if never used
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;

    $this->nodeStorage = $this->entityTypeManager->getStorage('node');
    $this->taxonomyTermStorage = $this->entityTypeManager->getStorage('taxonomy_term');
  }

  /**
   * @param \Drupal\commerce_product\Entity\ProductVariation $productVariation
   *
   * @return bool
   */
  private function productVariationHasWarrantyItem(ProductVariation $productVariation) {
    return $productVariation->hasField('field_warranty_item') && !$productVariation->get('field_warranty_item')->isEmpty();
  }

  /**
   * Returns the amount of warranty items for a single productVariation and the given $quantity
   *
   * @param \Drupal\commerce_product\Entity\ProductVariation $productVariation
   *
   * @return null|\Drupal\node\Entity\Node null if needs no warranty items, the node itself if it does
   */
  private function getProductVariationWarrantyItem(ProductVariation $productVariation) {
    if (!$this->productVariationHasWarrantyItem($productVariation)) {
      return null;
    }
    $warrantyItemVal = $productVariation->get('field_warranty_item')->first()->getValue();
    return $this->nodeStorage->load($warrantyItemVal['target_id']);
  }

  /**
   * Returns the max price for a single order for the Warranty adjustment
   *
   * @return float
   */
  private function getMaxOrderWarranty() {
    // set $totalWarrantyAmount to max the amount configged (if filled in)
    $maxPrice = $this->configFactory->get('mvi_guarantee.mviguaranteeconfig')->get('max_combined_guarantee_price');
    return $maxPrice ? floatval($maxPrice) : false;
  }

  /**
   * Returns the amount of warranty items for a single productVariation and the given $quantity
   *
   * @param \Drupal\commerce_product\Entity\ProductVariation $productVariation
   * @param int $quantity
   *
   * @return int
   */
  private function getAmountOfWarrantyItemsNeeded(ProductVariation $productVariation, $quantity) {
    if (!$this->productVariationHasWarrantyItem($productVariation)) return 0;

    $customUnit = $productVariation->get('field_custom_unit')->first()->getValue();
    $customUnitNode = $this->nodeStorage->load($customUnit['target_id']);

    $warrantyItem = $productVariation->get('field_warranty_item')->first()->getValue();
    $warrantyItemNode = $this->nodeStorage->load($warrantyItem['target_id']);
    $warrantyItemVolumneAmount = $warrantyItemNode->get('field_volume_amount')->first()->getValue();
    $volumeInSingleWarrantyItem = floatval($warrantyItemVolumneAmount['value']);

    $volumeAmount = $customUnitNode->get('field_volume_amount')->first()->getValue();
    $volumneNeeded = floatval($volumeAmount['value']) * $quantity;

    return ceil($volumneNeeded / $volumeInSingleWarrantyItem);
  }

  /**
   * Calculates the total amount of warranty for the given order
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   The order to set the adjustment for
   *
   * @return \Drupal\commerce_price\Price
   */
  public function getTotalWarrantyAmount(Order $order) {
    $orderItems = $order->getItems();
    $currencyCode = 'EUR';
    $totalWarrantyAmount = 0;
    foreach ($orderItems as $orderItem) {
      $productVariation = $orderItem->getPurchasedEntity();
      if ($this->productVariationHasWarrantyItem($productVariation)) {
        $customUnit = $productVariation->get('field_custom_unit')->first()->getValue();
        $customUnitNode = $this->nodeStorage->load($customUnit['target_id']);

        $warrantyItem = $productVariation->get('field_warranty_item')->first()->getValue();
        $warrantyItemNode = $this->nodeStorage->load($warrantyItem['target_id']);
        $warrantyItemPriceArr = $warrantyItemNode->get('field_price')->first()->getValue();
        $priceForSingleWarrantyItem = floatval($warrantyItemPriceArr['number']);
        $warrantyItemVolumneAmount = $warrantyItemNode->get('field_volume_amount')->first()->getValue();
        $volumeInSingleWarrantyItem = floatval($warrantyItemVolumneAmount['value']);

        $volumeAmount = $customUnitNode->get('field_volume_amount')->first()->getValue();
        $volumneNeeded = floatval($volumeAmount['value']) * $orderItem->getQuantity();

        $amountOfWarrantyItemsNeeded = ceil($volumneNeeded / $volumeInSingleWarrantyItem);

        $totalWarrantyAmount += ($amountOfWarrantyItemsNeeded * $priceForSingleWarrantyItem);

        $currencyCode = $warrantyItemPriceArr['currency_code'];
      }
    }

    // set $totalWarrantyAmount to max the amount configged (if filled in)
    $maxPrice = $this->getMaxOrderWarranty();
    if ($maxPrice && $maxPrice < $totalWarrantyAmount) {
      $totalWarrantyAmount = $maxPrice;
    }

    return new Price($totalWarrantyAmount, $currencyCode);
  }

  /**
   * Calculates the needed warranty adjustment and adds or removes it from/to the given order
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   The order to set the adjustment for
   */
  public function setGuaranteeAdjustment(Order $order)
  {
    $totalWarrantyPrice = $this->getTotalWarrantyAmount($order);

    if (floatval($totalWarrantyPrice->getNumber()) <= 0) {
      $currentGuaranteeAdjustments = $order->getAdjustments(['mvi_guarantee.guarantee']);

      if (isset($currentGuaranteeAdjustments[0])) {
        $order->removeAdjustment($currentGuaranteeAdjustments[0]);
        $order->save();
        return;
      }
    } else {
      $currentGuaranteeAdjustments = $order->getAdjustments(['mvi_guarantee.guarantee']);
      $addAdjustment = true;
      if (isset($currentGuaranteeAdjustments[0])) {
        $currentGuaranteeAdjustment = $currentGuaranteeAdjustments[0];
        // remove if needed, if not needed, no not add adjustment
        $currentAmount = $currentGuaranteeAdjustment->getAmount();

        // if amount is same
        if ($currentAmount->compareTo($totalWarrantyPrice) === 0) {
          $addAdjustment = false;
        } else {
          // if amount if different
          $order->removeAdjustment($currentGuaranteeAdjustment);
        }
      }

      if ($addAdjustment) {
        $adjustment = new Adjustment([
          'type' => 'mvi_guarantee.guarantee',
          'label' => t('Warranty'),
          'amount' => $totalWarrantyPrice,
          'locked' => true
        ]);
        $order->addAdjustment($adjustment);
        $order->save();
        return;
      }
    }
  }

  /**
   * Generates an array of associative arrays
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   The order to generate the labels for
   *
   * @return array[] associative array with 4 keys per item:
   *  order_id : '25' for example
   *  product_name: 'Soup of the day' for example
   *  warranty_item_abbreviation: 'SC' (from 'Soup Container') for example
   *  warranty_item_index: 1/3 (first from one out of 3 containers) for example
   */
  public function getLabelsForExport (Order $order) {
    $result = [];
    $orderItems = $order->getItems();
    foreach ($orderItems as $orderItem) {
      /** @var \Drupal\commerce_product\Entity\ProductVariation */
      $productVariation = $orderItem->getPurchasedEntity();
      if ($this->productVariationHasWarrantyItem($productVariation)) {
        $warrantyItem = $this->getProductVariationWarrantyItem($productVariation);
        $abbreviationVal = $warrantyItem->get('field_abbreviation')->first()->getValue();

        $amountOfWarrantyItemsNeeded = intval($this->getAmountOfWarrantyItemsNeeded($productVariation, $orderItem->getQuantity()));
        for ($i = 0; $i < $amountOfWarrantyItemsNeeded; $i++) {
          $result[] = [
            'order_id' => $order->id(),
            'product_name' => $productVariation->getProduct()->label(),
            'warranty_item_abbreviation' => $abbreviationVal['value'],
            'warranty_item_index' => ($i+1) . '/' . $amountOfWarrantyItemsNeeded,
          ];
        }
      }
    }
    return $result;
  }

  /**
   * Generates an array of associative arrays
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *   The order to generate the labels for
   *
   * @return array[] array where key is warranty item id, value is associative array with 3 keys per item:
   *  warrenty_item: 'Soup of the day' for example
   *  count: 5 for example
   *  price: commerce_price Price class instance!
   */
  public function getWarrantyItems(Order $order) {
    $result = [];
    $orderItems = $order->getItems();
    foreach ($orderItems as $orderItem) {
      $result = $this->getWarrantyItem($orderItem);
    }
    return $result;
  }

  public function getWarrantyItem(OrderItem $orderItem) {
    $result = [];

    /** @var \Drupal\commerce_product\Entity\ProductVariation */
    $productVariation = $orderItem->getPurchasedEntity();
    if ($this->productVariationHasWarrantyItem($productVariation)) {
      $warrantyItem = $this->getProductVariationWarrantyItem($productVariation);
      $warrantyItemId = $warrantyItem->id();
      $amountOfWarrantyItemsNeeded = intval($this->getAmountOfWarrantyItemsNeeded($productVariation, $orderItem->getQuantity()));
      if (isset($result[$warrantyItemId])) {
        $result[$warrantyItemId]['warranty_item_count'] += $amountOfWarrantyItemsNeeded;
      } else {
        $warrantyItemPriceArr = $warrantyItem->get('field_price')->first()->getValue();
        $result[$warrantyItemId] = [
          'warrenty_item' => $warrantyItem,
          'count' => $amountOfWarrantyItemsNeeded,
          'price' => new Price($warrantyItemPriceArr['number'], $warrantyItemPriceArr['currency_code'])
        ];
      }
    }

    return $result;
  }
}
