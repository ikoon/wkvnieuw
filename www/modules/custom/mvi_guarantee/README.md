# MVI Commerce Warranty README

## Installation
- Enable this module (MVI Commerce Pickup AND MVI Commerce Custom Unit should also install, make sure to read the README from those modules as well), you will notice the following:
	- MVI Commerce Pickup is installed, check the README.md there!
	- MVI Custom Unit is installed, check the README.md there!
	- 1 new Content type:
		- Warranty item : create as many of these as you want. These are the items that are given with certain products to be returned later.
	- 1 new field "Warranty Item" that is added to the default Product Variation Type. Add this to other Product Variation Types where needed.
	- A new config form at /admin/config/mvi_guarantee/mviguaranteeconfig ("access administration pages" permissions needed), where the maximum price for a single order for the warranty should be (defaults to €75)
	- A new action button at the orders overview "Export Warranty Item labels", where you can select the pickup time (defaults to today) and 1 or multiple orders states to export, click "Export labels" here.
		- Here you will see an overview with all "Warranty Items", needed for all orders for the selected criteria in previous form. Click "+ print" to trigger a print, the Warranty Items will get a simple label style when printing
	- A new tab when looking at the overview of an order (/admin/commerce/orders) called "Orders with Warranty Items", with an overview of all orders and a summary of all Warranty Items linked to the order. Printen out with label, amount, price and a total price for the order.


## Example
A Product with name 'Soup of the day' is packaged in a 'Soup container'. The 'Soup of the day' is 0.5L of soup, the 'Soup container' can hold a maximum of 1L of fluids. The warranty price for this 'Soup container' is €1 Translates to:

- Create a new "Warranty Item":
	- Title: "Soup container"
	- Price: 1
	- Abbreviation: SC
	- Volume amount: 1
	- Volume unit: "L"
- Create the Product 'Soup of the day'
	- At "Warranty Info", click "Add node":
		- Warranty Item: "Soup container"
		- Volume amount: 0.5
		- Volume unit: "L"
	- Fill in the rest of the product's fields and save
	- Put a couple of the 'Soup of the day' product in the shopping cart and go to checkout, you will notice the "Warranty" adjustment was added

