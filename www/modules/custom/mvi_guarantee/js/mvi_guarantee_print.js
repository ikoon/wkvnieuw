(function ($, window) {
  "use strict";
  /**
   * Export labels print
   */
  Drupal.behaviors.mviGuaranteePrint = {
    attach: function (context, settings) {
      $(context).find('[data-js-print]').not('[data-js-print-initted]').each(function () {
        $(this).attr('data-js-print-initted', '').on('click', function (e) {
          e.preventDefault();
          window.print();
        });
      });
    }
  };


})(jQuery, window);
