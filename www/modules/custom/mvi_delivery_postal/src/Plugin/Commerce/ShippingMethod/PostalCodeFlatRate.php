<?php

namespace Drupal\mvi_delivery_postal\Plugin\Commerce\ShippingMethod;

use Drupal\address\Plugin\Field\FieldType\AddressItem;
use Drupal\commerce_price\Price;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodBase;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\SupportsTrackingInterface;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\commerce_shipping\ShippingService;
use Drupal\Core\Url;
use Drupal\mvi_delivery_postal\PostalCodeResolver;
use Drupal\state_machine\WorkflowManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @CommerceShippingMethod(
 *   id = "postal_code_flat_rate",
 *   label = @Translation("Flat rate by postal code"),
 * )
 */

class PostalCodeFlatRate extends ShippingMethodBase implements SupportsTrackingInterface {

  /**
   * @var PostalCodeResolver;
   */
  protected $postalCodeResolver;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, PackageTypeManagerInterface $package_type_manager, WorkflowManagerInterface $workflow_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $package_type_manager, $workflow_manager);

    $this->services['default'] = new ShippingService('default', $this->t('Home delivery'));
    $this->configuration['rate_label'] = $this->t("Home delivery");
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.commerce_package_type'),
      $container->get('plugin.manager.workflow')
    );
    $instance->postalCodeResolver = $container->get('mvi_delivery_postal.resolver');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getTrackingUrl(ShipmentInterface $shipment) {
    $tracking_code = $shipment->getTrackingCode();
    if (!empty($tracking_code)) {
      return Url::fromUri('https://www.drupal.org/' . $tracking_code);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function calculateRates(ShipmentInterface $shipment)
  {
    $shippingInformation = $shipment->getShippingProfile();

    /* @var $address AddressItem */
    $address = $shippingInformation->get('address')->first();
    $postalCode = $address->getPostalCode();

    $orderTotal = $shipment->getOrder()->getTotalPrice();
    if(!$postalCode) {
      $shippingCost = null;
    } else {
      $shippingCost = $this->postalCodeResolver->returnShippingCostByPostalCode($postalCode, $orderTotal);
    }
    if(!$shippingCost) {
      $shippingCost = new Price('4.95', 'EUR');
    }

    $rates[] = new ShippingRate([
      'shipping_method_id' => $this->parentEntity->id(),
      'service' => $this->services['default'],
      'amount' => $shippingCost
    ]);

    return $rates;
  }
}
