<?php

namespace Drupal\mvi_delivery_postal\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'ValidatePostalCodeBlock' block.
 *
 * @Block(
 *  id = "validate_postal_code_block",
 *  admin_label = @Translation("Validate delivery postal code block"),
 * )
 */
class ValidatePostalCodeBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructs a new ValidatePOstalCodeBlock object.
   *
   * @param PostalCodeStorage $postal_code_storage
   *   The PostalCodeStorage definition.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    FormBuilderInterface $form_builder
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $form =  $this->formBuilder->getForm('Drupal\mvi_delivery_postal\Form\ValidatePostalCodeForm');

    $build = [];
    $build['#theme'] = 'validate_postal_code_block';
    $build['#cache'] = ['max-age' => 0];
    $build['#form'] = $form;

    return $build;
  }

}
