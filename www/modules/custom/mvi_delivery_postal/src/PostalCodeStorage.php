<?php

namespace Drupal\mvi_delivery_postal;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\NodeStorageInterface;

/**
 * Class PostalCodeStorage
 *
 * @package Drupal\mvi_delivery_postal
 */
class PostalCodeStorage {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new PostalCodeStorage object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  public function getAllowedPostalCodes() {
    /** @var NodeStorageInterface $nodeStorage */
    $nodeStorage = $this->entityTypeManager->getStorage('node');
    $postalCodes = $nodeStorage->loadByProperties(['type' => 'shipping_postal_code']);

    if(!$postalCodes) {
      return null;
    }

    $postalCodesArr = [];
    foreach ($postalCodes as $postalCode) {
      $postalCodesArr[] = $postalCode->field_postal_code->value;
    }

    return $postalCodesArr;
  }

  public function getShippingCostByPostalCode($postalCode) {
    /** @var NodeStorageInterface $nodeStorage */
    $nodeStorage = $this->entityTypeManager->getStorage('node');
    $shippingCost = $nodeStorage->loadByProperties(['type' => 'shipping_postal_code', 'field_postal_code' => $postalCode]);

    return $shippingCost;
  }

}
