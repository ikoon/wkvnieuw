<?php

namespace Drupal\mvi_delivery_postal\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\mvi_delivery_postal\PostalCodeResolver;
use Drupal\mvi_delivery_postal\PostalCodeStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;

class ValidatePostalCodeForm extends FormBase {

  /**
   * @var PostalCodeResolver;
   */
  protected $postalCodeResolver;

  public function __construct(PostalCodeResolver $postal_code_resolver) {
    $this->postalCodeResolver = $postal_code_resolver;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('mvi_delivery_postal.resolver')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mvi_delivery_postal_validate_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['postal_code'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Check if we deliver in your region'),
      '#placeholder' => $this->t('Fill in your postal code'),
      '#required' => true
    );

    $form['message'] = [
      '#type' => 'markup',
      '#markup' => '<div class="result-message"></div>'
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Search'),
      '#ajax' => [
        'callback' => '::validatePostalCode',
      ],
    ];

    return $form;

  }

  public function validatePostalCode(array &$form, FormStateInterface $form_state) {
    $postalCode = $form_state->getValue('postal_code');

    $response = new AjaxResponse();
    $response->addCommand(
      new ReplaceCommand(
        '.result-message',
        '<div class="result-message success">' . t('Yes, we do deliver in this region!')
      )
    );

    if(!$this->postalCodeResolver->isPostalCodeDeliverable($postalCode)) {
      $response->addCommand(
        new ReplaceCommand(
          '.result-message',
          '<div class="result-message error">' . t('Unfortunately, we do not deliver in this region!')
        )
      );
    }
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $postalCode = $form_state->getValue('postal_code');

    if($this->postalCodeResolver->isPostalCodeDeliverable($postalCode)) {
      // @todo: set this postalcode as default in the current session.
    }
    return true;
  }
}
