<?php

namespace Drupal\mvi_delivery_postal;

use Drupal\commerce_price\Price;

/**
 * Class PostalCodeResolver
 * @package Drupal\mvi_delivery_postal
 */
class PostalCodeResolver {

  /**
   * @var PostalCodeStorage;
   */
  protected $storage;

  public function __construct(PostalCodeStorage $storage) {
    $this->storage = $storage;
  }

  /**
   * @param $postalCode
   * @return bool true if webshop delivers to this postal code, false if not.
   */
  public function isPostalCodeDeliverable($postalCode) {
    $allowedPostalCodes = $this->storage->getAllowedPostalCodes();
    if(!in_array($postalCode, $allowedPostalCodes)) {
      return false;
    }
    return true;
  }


  /**
   * @param string $postalCode
   * @param Price $orderTotal
   * @return bool|Price false if no shipping cost is found for this postal code | the shipping cost based on the postal code and order total.
   */
  public function returnShippingCostByPostalCode($postalCode, $orderTotal) {
    $result = $this->storage->getShippingCostByPostalCode($postalCode);
    $node = reset($result);
    if(!$node) {
      return false;
    }

    $price = $node->get('field_shipping_price')->first()->getValue();
    $freeFrom = $node->get('field_free_shipping_from_price')->first()->getValue();
    $freeFrom = new Price ($freeFrom['number'], $freeFrom['currency_code']);

    $shippingCost = new Price($price['number'], $price['currency_code']);
    if($orderTotal->greaterThan($freeFrom)) {
      $shippingCost = new Price('0', $price['currency_code']);
    }

    return $shippingCost;
  }
}
