<?php

namespace Drupal\mvi_pickup;

use Drupal\Core\Database\Connection;

/**
 * Class MviPickupHoursStorage
 *
 * @package Drupal\mvi_pickup
 */
class MviPickupHoursStorage implements MviPickupHoursStorageInterface {

    protected $database;

    /**
     * @param \Drupal\Core\Database\Connection $connection
     */
    public function __construct(Connection $database) {
        $this->database = $database;
    }

    public function update($day, $start = null, $end = null)
    {
        $this->database->update('mvi_pickup_openinghours')
            ->fields(['start' => $start, 'end' => $end])
            ->condition('day', $day)
            ->execute();
    }

    public function select($day)
    {
        $query = $this->database->select('mvi_pickup_openinghours', 'hours');
        $query->fields('hours', ['start', 'end']);
        $query->condition('day', $day);
        return $query->execute()->fetch();
    }

    public function selectAll()
    {
        $query = $this->database->select('mvi_pickup_openinghours', 'hours');
        $query->fields('hours', ['start', 'end', 'day']);
        return $query->execute()->fetchAllAssoc('day');
    }

    public function delete($id)
    {
        $this->database->delete('mvi_pickup_openinghours')
            ->condition('id', $id)
            ->execute();
    }
}