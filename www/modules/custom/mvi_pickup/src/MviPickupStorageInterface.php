<?php

namespace Drupal\mvi_pickup;

/**
 * Interface MviPickupStorageInterface
 *
 * @package Drupal\mci_pickup
 */
interface MviPickupStorageInterface {
    /**
     * @param $id
     * @param $uid
     * @return mixed
     */
    public function add($date, $start, $end);

    /**
     * @return mixed
     */
    public function select();

    /**
     * @param $id
     * @param $uid
     * @return mixed
     */
    public function delete($id);
}
