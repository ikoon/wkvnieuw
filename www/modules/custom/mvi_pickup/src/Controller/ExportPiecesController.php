<?php

namespace Drupal\mvi_pickup\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;

use Drupal\commerce_order\Entity\Order;

use Exception;

/**
 * Class ExportPiecesController.
 */
class ExportPiecesController extends ControllerBase {

  /**
   * Page.
   *
   * @return string
   *   Return Hello string.
   */
  public function page() {
    $from = \Drupal::request()->query->get('from');
    $to = \Drupal::request()->query->get('to');
    $states = \Drupal::request()->query->get('states');
    if (empty($from) || empty($to) || empty($states)) {
      throw new Exception('Invalid query params');
    }
    
    $fromTimestamp = DrupalDateTime::createFromTimestamp($from);
    $toTimestamp = DrupalDateTime::createFromTimestamp($to);

    $query = \Drupal::entityQuery('commerce_order')
              ->condition('state', explode(';;', $states), 'in')
              ->condition('field_pickup_from', $from, '>=')
              ->condition('field_pickup_from', $to, '<=');
    $entity_ids = $query->execute();
    $orders = Order::loadMultiple($entity_ids);
    $items = [];
    $columns = [t('Order number'), t('Product'), t('Quantity')];
    foreach ($orders as $order) {
      $orderItems = $order->getItems();
      foreach ($orderItems as $orderItem) {
        $items[] = [
           $order->id(),
           $orderItem->getPurchasedEntity()->label(),
           $orderItem->getQuantity(),
        ];
      }
    }

    return [
      '#theme' => 'mvi_pickup_export_pieces_print',
      '#items' => $items,
      '#from' => $fromTimestamp->format('d/m/Y H:i'),
      '#to' => $toTimestamp->format('d/m/Y H:i'),
      '#columns' => $columns,
      '#attached' => [
        'library' => [
          'mvi_pickup/mvi_pickup_export_pieces_print'
        ]
      ]
    ];
  }

}
