<?php

namespace Drupal\mvi_pickup\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\mvi_pickup\MviPickupStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MviPickupManage extends ControllerBase{

    protected $storage;

    public function __construct(MviPickupStorage $storage) {
        $this->storage = $storage;
    }

    public static function create(ContainerInterface $container) {
        return new static(
            $container->get('mvi_pickup.storage')
        );
    }

    public function managePage(){

        $form = \Drupal::formBuilder()->getForm('Drupal\mvi_pickup\Form\PickupManageSettingsForm');

        $build = [
            '#theme' => 'mvi_pickup_manage',
            '#form' => $form
        ];

        return $build;
    }
}