<?php

namespace Drupal\mvi_pickup;

/**
 * Interface MviPickupStorageInterface
 *
 * @package Drupal\mci_pickup
 */
interface MviPickupHoursStorageInterface {

    /**
     * @param $day
     * @param $start
     * @param $end
     * @return mixed
     */
    public function update($day, $start = null, $end = null);

    /**
     * @param $day
     * @return mixed
     */
    public function select($day);

    /**
     * @return array
     */
    public function selectAll();

    /**
     * @param $day
     * @return mixed
     */
    public function delete($day);
}
