<?php

namespace Drupal\mvi_pickup\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\mvi_pickup\MviPickupRelativeMinPickupTimeStorage;
use Drupal\mvi_pickup\MviPickupHoursStorage;
use Drupal\mvi_pickup\MviPickupResolver;
use Drupal\mvi_pickup\MviPickupStorage;

/**
 * Provides a 'SelectPickupTimesBlock' block.
 *
 * @Block(
 *  id = "select_pickup_times_block",
 *  admin_label = @Translation("Select pickup times block"),
 * )
 */
class SelectPickupTimesBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\mvi_pickup\MviPickupRelativeMinPickupTimeStorage
   */
  protected $timesStorage;

  /**
   * @var \Drupal\mvi_pickup\MviPickupHoursStorage
   */
  protected $hoursStorage;

  /**
   * @var Drupal\mvi_pickup\MviPickupStorage
   */
  protected $exceptionsStorage;

  /**
   * @var Drupal\mvi_pickup\MviPickupResolver
   */
  protected $resolver;

  /**
   * Constructs a new SelectPickupTimesBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\mvi_pickup\MviPickupRelativeMinPickupTimeStorage $times_storage
   *   The MviPickupRelativeMinPickupTimeStorage definition.
   * @param \Drupal\mvi_pickup\MviDeliveryAvailabilityStorage $hours_storage
   *   The MviPickupRelativeMinPickupTimeStorage definition.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    MviPickupRelativeMinPickupTimeStorage $times_storage,
    MviDeliveryAvailabilityStorage $hours_storage,
    MviDeliveryStorage $exceptions_storage,
    MviPickupResolver $resolver
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->timesStorage = $times_storage;
    $this->hoursStorage = $hours_storage;
    $this->exceptionsStorage = $exceptions_storage;
    $this->resolver = $resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('mvi_pickup.rel_min_pickup_time_storage'),
      $container->get('mvi_pickup.hours_storage'),
      $container->get('mvi_pickup.storage'),
      $container->get('mvi_pickup.pickup_resolver')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['#theme'] = 'select_pickup_times_block';
    $build['#cache'] = ['max-age' => 0];

    $form = \Drupal::formBuilder()->getForm('Drupal\mvi_pickup\Form\MviPickupForm');
    $build['#form'] = $form;

    return $build;
  }

}
