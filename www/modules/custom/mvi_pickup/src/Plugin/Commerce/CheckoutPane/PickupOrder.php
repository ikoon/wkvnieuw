<?php

namespace Drupal\mvi_pickup\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\mvi_pickup\MviPickupHoursStorage;
use Drupal\mvi_pickup\MviPickupRelativeMinPickupTimeStorage;
use Drupal\mvi_pickup\MviPickupStorage;
use Drupal\mvi_pickup\MviPickupResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the shipping information pane.
 *
 * Collects the shipping profile, then the information for each shipment.
 * Assumes that all shipments share the same shipping profile.
 *
 * @CommerceCheckoutPane(
 *   id = "pickup_order",
 *   label = @Translation("Pick up order"),
 *   wrapper_element = "fieldset",
 * )
 */
class PickupOrder extends CheckoutPaneBase implements ContainerFactoryPluginInterface
{

  /**
   * Constructs a new CheckoutPaneBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface $checkout_flow
   *   The parent checkout flow.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\mvi_pickup\MviPickupRelativeMinPickupTimeStorage $timing_storage
   *   The pickup timing storage
   * @param \Drupal\mvi_pickup\MviPickupHoursStorage $hours_storage
   *   The opening hours storage
   * @param \Drupal\mvi_pickup\MviPickupStorage $exception_storage
   *   The exceptions storage
   * @param \Drupal\mvi_pickup\MviPickupResolver $resolver
   *   The pickup resolver
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow, EntityTypeManagerInterface $entity_type_manager, MviPickupRelativeMinPickupTimeStorage $timing_storage, MviPickupHoursStorage $hours_storage, MviPickupStorage $exception_storage, MviPickupResolver $resolver)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $checkout_flow, $entity_type_manager);

    $this->checkoutFlow = $checkout_flow;
    $this->order = $checkout_flow->getOrder();
    $this->setConfiguration($configuration);

    $this->entityTypeManager = $entity_type_manager;
    $this->timingStorage = $timing_storage;
    $this->hoursStorage = $hours_storage;
    $this->exceptionStorage = $exception_storage;
    $this->resolver = $resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $checkout_flow,
      $container->get('entity_type.manager'),
      $container->get('mvi_pickup.rel_min_pickup_time_storage'),
      $container->get('mvi_pickup.hours_storage'),
      $container->get('mvi_pickup.storage'),
      $container->get('mvi_pickup.pickup_resolver')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneSummary() {

    $summary = null;
    $date = $this->order->get('field_pickup_from')->getValue();

    if($date) {
      $from = $this->order->get('field_pickup_from')->getValue();
      $to = $this->order->get('field_pickup_to')->getValue();

      $summary = $this->t("Pickup on:") . " <strong>" . date('d/m/Y', $date[0]['value']) . "</strong> " . $this->t('between') . " <strong>" . date('H:i', $from[0]['value']) . "</strong>" . $this->t('and') . "<strong>" . date('H:i',$to[0]['value']) . '</strong>';
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form)
  {
    $this->resolver->addPickupTimesToForm($pane_form, $this->order);
    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function validatePaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {

    $shippingInformation = $form_state->getValue('mvi_shipping_information');
    $shippingMethod = $shippingInformation["shipments"][0]["shipping_method"][0];
    // When the pickup order shipping method is selected, require the stard and end fields.
    if($shippingMethod == '2--default') {
      $pickupData = $form_state->getValue('pickup_order');

      if(!$pickupData) {
        $form_state->setErrorByName('pickup_order', 'Gelieve een ophaalmoment op te geven.');
      }

      if(!$pickupData['pickup_order']['date']) {
        $form_state->setErrorByName('pickup_order', 'Gelieve een ophaalmoment op te geven.');
      }

      if(!$pickupData['pickup_order']['between']) {
        $form_state->setErrorByName('pickup_order', 'Gelieve een uur te kiezen bij het ophaalmoment.');
      }

      if(!$pickupData['pickup_order']['between']['end'] || !$pickupData['pickup_order']['between']['start']) {
        $form_state->setErrorByName('pickup_order', 'Gelieve een uur te kiezen bij het ophaalmoment.');
      }

      $errors = $this->resolver->getPickupTimesErrors(
        $pickupData['pickup_order']['date'],
        $pickupData['pickup_order']['between']['start'],
        $pickupData['pickup_order']['between']['end']
      );

      if ($errors) {
        foreach($errors as $messages) {
          foreach($messages as $msg) {
            $form_state->setErrorByName('pickup_order', $msg);
          }
        }
      }
    }
  }

  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $shippingInformation = $form_state->getValue('mvi_shipping_information');
    $shippingMethod = $shippingInformation["shipments"][0]["shipping_method"][0];
    // When the pickup order shipping method is selected, require the stard and end fields.
    if($shippingMethod == '2--default') {
      $pickupData = $form_state->getValue('pickup_order');

      $this->resolver->updatePickupTimes(
        $pickupData['pickup_order']['date'],
        $pickupData['pickup_order']['between']['start'],
        $pickupData['pickup_order']['between']['end'],
        $this->order
      );
    } else {
      $this->resolver->updatePickupTimes(
        null,
        null,
        null,
        $this->order
      );
    }
  }

}
