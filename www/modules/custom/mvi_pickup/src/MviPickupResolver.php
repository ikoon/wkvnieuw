<?php

namespace Drupal\mvi_pickup;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Session\AccountProxy;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_store\CurrentStore;
use Drupal\commerce_cart\CartProvider;

use Drupal\mvi_pickup\MviPickupStorageInterface;
use Drupal\mvi_pickup\MviPickupHoursStorageInterface;
use Drupal\mvi_pickup\MviPickupRelativeMinPickupTimeStorage;

/**
 * Class MviPickupResolver.
 */
class MviPickupResolver
{
  use StringTranslationTrait;

  /**
   * @var \Drupal\mvi_pickup\MviPickupStorageInterface
   */
  protected $exceptionStorage;

  /**
   * @var \Drupal\mvi_pickup\MviPickupHoursStorageInterface
   */
  protected $hoursStorage;

  /**
   * @var \Drupal\mvi_pickup\MviPickupRelativeMinPickupTimeStorage
   */
  protected $timingStorage;

  /**
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * @var \Drupal\commerce_store\CurrentStore
   */
  protected $current_store;

  /**
   * @var \Drupal\commerce_cart\CartProvider
   */
  protected $cart_provider;

  /**
   * Constructs a new MviPickupResolver object.
   */
  public function __construct(
    MviPickupStorageInterface             $mvi_pickup_storage,
    MviPickupHoursStorageInterface        $mvi_pickup_hours_storage,
    MviPickupRelativeMinPickupTimeStorage $mvi_pickup_rel_min_pickup_time_storage,
    AccountProxy                          $current_user,
    CurrentStore                          $current_store,
    CartProvider                          $cart_provider
  )
  {
    $this->exceptionStorage = $mvi_pickup_storage;
    $this->hoursStorage = $mvi_pickup_hours_storage;
    $this->timingStorage = $mvi_pickup_rel_min_pickup_time_storage;
    $this->currentUser = $current_user;
    $this->currentStore = $current_store;
    $this->cartProvider = $cart_provider;
  }

  /**
   * @param \Drupal\commerce_product\Entity\Product $product
   *
   * @return true|\TimeStamp true if can be ordered, the timestamp from when it can be ordered if can not be ordered
   */
  public function canProductBeOrdered(Product $product, Order $order = null)
  {
    $relativeTimestamp = $this->timingStorage->getRelativeTimestampFromProduct($product);
    if (is_null($relativeTimestamp)) {
      return true;
    }

    if (is_null($order)) {
      $order = $this->getCart();
    }
    $currentStartDateTime = $this->getPickupStartDate($order);
    if (is_null($currentStartDateTime)) {
      return true;
    }

    $relativeDate = date('Y/m/d H:i', $relativeTimestamp);
    $relativeDateTime = \DateTime::createFromFormat('Y/m/d H:i', $relativeDate);

    if ($relativeDateTime->getTimestamp() < $currentStartDateTime->getTimestamp()) {
      return true;
    }
    return $relativeDateTime;
  }

  /**
   * @return \Drupal\commerce_order\Entity\Order the current cart of type 'default' (or a new one if the user did not have one already)
   */
  public function getCart()
  {
    $store = $this->currentStore->getStore();
    $cart = $this->cartProvider->getCart('default', $store);
    if (!$cart) {
      $cart = $this->cartProvider->createCart('default', $store);
    }
    return $cart;
  }

  /**
   * @param \Drupal\commerce_order\Entity\Order $order
   *
   * @return null|\DateTime null if none was found, a PHP DateTime object otherwise
   */
  private function getPickupStartDate(Order $order)
  {
    if (!$order->hasField('field_pickup_from')) {
      return null;
    }

    if ($order->get('field_pickup_from')->isEmpty()) {
      return null;
    }

    $val = $order->get('field_pickup_from')->first()->getValue();
    if ($from = $val['value']) {
      // If the date is before now, reset the date to now. Otherwise, the date pickup calendar wil set a date in
      // the past as default value.
      $now = date('Y/m/d H:i');
      $strDate = date('Y/m/d H:i', $from);
      if ($strDate < $now) {
        $strDate = $now;
      }
      return \DateTime::createFromFormat('Y/m/d H:i', $strDate);
    }

    return null;
  }


  /**
   * @param \Drupal\commerce_order\Entity\Order $order
   *
   * @return null|\DateTime null if none was found, a PHP DateTime object otherwise
   */
  private function getPickupEndDate(Order $order)
  {
    if (!$order->hasField('field_pickup_to')) {
      return null;
    }

    if ($order->get('field_pickup_to')->isEmpty()) {
      return null;
    }

    $val = $order->get('field_pickup_to')->first()->getValue();
    if ($from = $val['value']) {
      $strDate = date('Y/m/d H:i', $from);
      return \DateTime::createFromFormat('Y/m/d H:i', $strDate);
    }

    return null;
  }

  /**
   * Adds the date & between start/end fields (& the JS-libaries) to the given form
   *
   * @param array $form
   * @param \Drupal\commerce_order\Entity\Order $order
   *
   * @return array The updated form
   */
  public function addPickupTimesToForm(array &$form, Order $order)
  {
    $currentStartTime = $this->getPickupStartDate($order);
    $currentEndTime = $this->getPickupEndDate($order);

    $form['pickup_order'] = [
      '#type' => 'container',
      '#attributes' => ['data-pickup-order' => '1'],
    ];

    $form['pickup_order']['date'] = [
      '#title' => $this->t('Date'),
      '#type' => 'textfield',
      '#placeholder' => $this->t('YYYY/mm/dd'),
      '#attributes' => ['data-pickup-date' => '1'],
    ];

    $form['pickup_order']['between'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Hour'),
      '#attributes' => ['data-pickup-between' => '1'],
    );

    $form['pickup_order']['between']['start'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Start'),
      '#size' => 60,
      '#maxlength' => 128,
      '#prefix' => $this->t('Between '),
      '#suffix' => $this->t(' and '),
      '#placeholder' => $this->t('HH:mm'),
      '#attributes' => ['data-pickup-time-start' => '1'],
    );

    $form['pickup_order']['between']['end'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('End'),
      '#size' => 60,
      '#maxlength' => 128,
      '#placeholder' => $this->t('HH:mm'),
      '#attributes' => ['data-pickup-time-end' => '1'],
    );
    $now = new \DateTime();

    if (!is_null($currentStartTime) || !is_null($currentEndTime)) {
      if ($now->getTimestamp() < $currentStartTime->getTimestamp()) {
        $this->checkPickupTimeStillGood($order);
      }
    }
    if (!is_null($currentStartTime)) {
      if ($now->getTimestamp() < $currentStartTime->getTimestamp()) {
        $form['pickup_order']['date']['#default_value'] = $currentStartTime->format('Y/m/d');
        $form['pickup_order']['between']['start']['#default_value'] = $currentStartTime->format('H:i');
      }
    }
    if (!is_null($currentEndTime)) {
      if ($now->getTimestamp() < $currentStartTime->getTimestamp()) {
        $form['pickup_order']['between']['end']['#default_value'] = $currentEndTime->format('H:i');
      }
    }

    $openingHours = $this->hoursStorage->selectAll();
    $exceptions = $this->exceptionStorage->select();
    $earliestTimestamp = $this->timingStorage->selectEarliestTimestamp($order);
    $maximumTimestamp = $this->timingStorage->selectMaximumTimestamp();

    $form['#cache']['max-age'] = 0;
    $form['#attached']['library'][] = 'mvi_pickup/mvi_pickup_checkout';
    $form['#attached']['library'][] = 'mvi_pickup/mvi_pickup_pickadate';
    $form['#attached']['drupalSettings']['mvi_pickup']['openinghours'] = $openingHours;
    $form['#attached']['drupalSettings']['mvi_pickup']['exceptions'] = $exceptions;
    $form['#attached']['drupalSettings']['mvi_pickup']['earliestTimestamp'] = $earliestTimestamp;
    $form['#attached']['drupalSettings']['mvi_pickup']['maximumTimestamp'] = $maximumTimestamp;

    return $form;
  }

  /**
   * @param string $strDate as 'Y/m/d' - example: '2020/08/20'
   * @param string $strStart as 'H:i' - example: '08:00'
   * @param string $strEnd as 'H:i' - example: '20:00'
   * @param Order $order
   *
   * @return null|array null if no values are given/no errors occurred.Or an associative array with 1 key per field ('date', 'start' or 'end') and the value is an array of error messages.
   */
  public function getPickupTimesErrors($strDate, $strStart, $strEnd, $order = null)
  {
    if (!($strDate && $strStart && $strEnd)) {
      return null;
    }

    $date = \DateTime::createFromFormat('Y/m/d', $strDate);
    $from = \DateTime::createFromFormat('Y/m/d H:i', $strDate . ' ' . $strStart);
    $to = \DateTime::createFromFormat('Y/m/d H:i', $strDate . ' ' . $strEnd);

    $result = [];
    if (!$date) {
      $result['date'][] = $this->t('Invalid date date "@date" given, please provide a date in the form YYYY/mm/dd.', ['@date' => $strDate]);
    }

    if (!$from) {
      $result['start'][] = $this->t('Invalid date from time "@from" given, please provide a time in the form HH:mm.', ['@from' => $strStart]);
    }

    if (!$to) {
      $result['end'][] = $this->t('Invalid date to time "@strEnd" given, please provide a time in the form HH:mm.', ['@strEnd' => $strEnd]);
    }

    if (count($result) > 0) {
      return $result;
    }

    $fromTimestamp = $from->getTimestamp();
    $toTimestamp = $to->getTimestamp();

    // check if enddate is more then max
    $max = $this->timingStorage->selectMaximumTimestamp();
    if ($max && $toTimestamp > $max) {
      $result['date'][] = $this->t('The end date & time "@to" is invalid.', ['@to' => $to->format('Y/m/d H:i')]);
      return $result;
    }

    // check if $min is equal or less to earliest timestamp
    $min = $this->timingStorage->selectEarliestTimestamp($order);
    if ($min && isset($min['timestamp']) && $fromTimestamp < $min['timestamp']) {
      $result['date'][] = $this->t('The start date & time "@from" is invalid.', ['@from' => $from->format('Y/m/d H:i')]);
      return $result;
    }

    // if max time is smaller then min time, error!
    if ($toTimestamp < $fromTimestamp) {
      $result['end'][] = $this->t('The start time can not be bigger then the end time.');
    }

    // check the openinghours $from and $to
    $allHours = $this->hoursStorage->selectAll();
    $dayOfWeek = strtolower($date->format('l'));
    $exceptions = $this->exceptionStorage->select();

    if (isset($allHours[$dayOfWeek])) {
      // is there an exception for this date
      $isException = false;
      foreach ($exceptions as $exception) {
        $exDate = new \DateTime();
        $exDate->setTimestamp($exception->date);
        if ($date->format('Y-m-d') == $exDate->format('Y-m-d')) {
          $isException = true;
          break;
        }
      }

      if ($isException) {
        $minTime = $exception->start;
        $maxTime = $exception->end;
      } else {
        $minTime = $allHours[$dayOfWeek]->start;
        $maxTime = $allHours[$dayOfWeek]->end;
      }

      // is closed this day
      if ($minTime === $maxTime) {
        $result['date'][] = $this->t('Invalid date. On the given date "@date", the shop is closed.', ['@date' => $from->format('Y/m/d')]);
        return $result;
      }

      // pickup time earlier then opening hours minimum
      $minTimeAsDate = \DateTime::createFromFormat('Y/m/d H:i:s', $strDate . ' ' . $minTime);
      if ($fromTimestamp < $minTimeAsDate->getTimestamp()) {
        $result['start'][] = $this->t('At the given start time "@from", the shop is not opened yet.', ['@from' => $from->format('H:i')]);
      }

      // pickup time later then opening hours maximum
      $maxTimeAsDate = \DateTime::createFromFormat('Y/m/d H:i:s', $strDate . ' ' . $maxTime);
      if ($toTimestamp > $maxTimeAsDate->getTimestamp()) {
        $result['end'][] = $this->t('At the given end time "@to", the shop is closed.', ['@to' => $to->format('H:i')]);
      }

    }

    return count($result) > 0 ? $result : null;
  }

  /**
   * @param string $strDate as 'Y/m/d' - example: '2020/08/20'
   * @param string $strStart as 'H:i' - example: '08:00'
   * @param string $strEnd as 'H:i' - example: '20:00'
   * @param \Drupal\commerce_order\Entity\Order $order
   */
  public function updatePickupTimes($strDate, $strStart, $strEnd, Order $order)
  {
    $errors = $this->getPickupTimesErrors($strDate, $strStart, $strEnd);
    if (!is_null($errors)) {
      throw new \Exception('Can not save invalid data. Date: ' . $strDate . ', start: ' . $strStart . ', end: ' . $strEnd);
    }

    if ($strDate && $strStart && $strEnd) {
      $from = \DateTime::createFromFormat('Y/m/d H:i', $strDate . ' ' . $strStart);
      $to = \DateTime::createFromFormat('Y/m/d H:i', $strDate . ' ' . $strEnd);

      $order->set('field_pickup_from', $from->getTimestamp());
      $order->set('field_pickup_to', $to->getTimestamp());
    } else {
      $order->set('field_pickup_from', null);
      $order->set('field_pickup_to', null);
    }
    $order->save();
  }

  /**
   * Shows a drupal message when one or more products in the shopping cart can not be picked up at the chosen time
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *
   * @return boolean true it the given pickup time is valid (or if there is none), false if not
   */
  public function checkPickupTimeStillGood(Order $order)
  {
    if (!$order) return true;

    $result = true;

    $orderItems = $order->getItems();
    foreach ($orderItems as $orderItem) {
      $product = $orderItem->getPurchasedEntity()->getProduct();
      $timestamp = $this->canProductBeOrdered($product);
      if ($timestamp !== true) {
        \Drupal::messenger()->addError(t('The product "@product" can not be ordered untill @datetime. Please remove the product from the cart or update the pickup time to be able to complete your order.', [
          '@product' => $orderItem->getTitle(),
          '@datetime' => $timestamp->format('Y/m/d H:i'),
        ]));
        $result = false;
      }
    }
    return $result;
  }
}
