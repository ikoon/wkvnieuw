<?php

namespace Drupal\mvi_pickup\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mvi_pickup\MviPickupHoursStorage;
use Drupal\mvi_pickup\MviPickupRelativeMinPickupTimeStorage;
use Drupal\mvi_pickup\MviPickupStorage;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MviPickupManageHoursForm extends FormBase {

    protected $hoursStorage;
    protected $storage;
    protected $relativeMinPickupTimeStorage;

    public function __construct(MviPickupHoursStorage $hoursStorage, MviPickupStorage $storage, MviPickupRelativeMinPickupTimeStorage $relative_min_pickup_time_storage) {
        $this->hoursStorage = $hoursStorage;
        $this->storage = $storage;
        $this->relativeMinPickupTimeStorage = $relative_min_pickup_time_storage;
    }

    public static function create(ContainerInterface $container) {
        return new static(
            $container->get('mvi_pickup.hours_storage'),
            $container->get('mvi_pickup.storage'),
            $container->get('mvi_pickup.rel_min_pickup_time_storage')
        );
    }


    public function getFormId()
    {
        return 'mvi_pickup_manage_hours';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {

        $form['static_pickup_datetimes'] = [
          '#type' => 'fieldset',
          '#title' => $this->t('Global pickup restrictions'),
        ];
        
        $min = $this->storage->getStaticDateTime('min');
        $max = $this->storage->getStaticDateTime('max');
        $relativeMinId = $this->storage->getStaticDateTime('relative_min');
        $form['static_pickup_datetimes']['min'] = [
          '#type' => 'datetime',
          '#title' => $this->t('Minimum'),
          '#default_value' => $min ? new DrupalDateTime(date('Y-m-d H:i:s', $min)) : null,
          '#description' => $this->t('<strong>Before</strong> this date & time, customers can not pickup any products.'),
        ];
        $form['static_pickup_datetimes']['max'] = [
          '#type' => 'datetime',
          '#title' => $this->t('Maximum'),
          '#default_value' => $max ? new DrupalDateTime(date('Y-m-d H:i:s', $max)) : null,
          '#description' => $this->t('<strong>After</strong> this date & time, customers can not pickup any products.'),
        ];
        $form['static_pickup_datetimes']['relative_min'] = [
          '#type' => 'entity_autocomplete',
          '#title' => $this->t('Relative minimum pickup time'),
          '#description' => $this->t('Select one of the "Relatieve ophaalmomenten" (taxonomy term) to disable customers to select a pickup time, relative to now. If you select "2 hours" for example, a customer ordering at 08:00 will not be able to select a pickup time <strong>for ANY product</strong> before 10:00 .'),
          '#target_type' => 'taxonomy_term',
          '#selection_settings' => ['target_bundles' => ['mvi_relative_times']],
          '#size' => 30,
          '#maxlength' => 1024,
          '#default_value' => $relativeMinId ? Term::load($relativeMinId) : null
        ];
        $days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];

        foreach ($days as $key => $day) {
            $form[$day] = [
                '#type' => 'fieldset',
                '#title' => $this->t($day)
            ];
            $form[$day][$day . '_start'] = [
                '#type' => 'datetime',
                '#title' => $this->t('Van:'),
                '#default_value' => new DrupalDateTime($this->hoursStorage->select($day)->start),
                '#size' => 20,
                '#required' => TRUE,
                '#date_date_element' => 'none',
                '#date_time_element' => 'time',
            ];
            $form[$day][$day . '_end'] = [
                '#type' => 'datetime',
                '#title' => $this->t('Tot:'),
                '#default_value' => new DrupalDateTime($this->hoursStorage->select($day)->end),
                '#size' => 20,
                '#required' => TRUE,
                '#date_date_element' => 'none',
                '#date_time_element' => 'time',
            ];
        }

        $form['actions'] = ['#type' => 'actions'];
        $form['actions']['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Update'),
        );

        return $form;
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {

        $userInput = $form_state->getValues();

        if (is_null($userInput['relative_min'])) {
          $this->storage->setStaticDateTime('relative_min', null);
        } else if ($term = Term::load($userInput['relative_min'])) {
          $this->storage->setStaticDateTime('relative_min', intval($term->id()));
        }

        $min = $userInput['min'];
        $minStaticDateTime = $min ? strtotime($min->format('Y-m-d H:i:s')) : null;
        
        $max = $userInput['max'];
        $maxStaticDateTime = $max ? strtotime($max->format('Y-m-d H:i:s')) : null;

        $this->storage->setStaticDateTime('min', $minStaticDateTime);
        $this->storage->setStaticDateTime('max', $maxStaticDateTime);
      
        $days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
        foreach ($days as $key => $day){
            /* @var DrupalDateTime $start */
            $start = $userInput[$day . '_start'];
            /* @var DrupalDateTime $end */
            $end = $userInput[$day . '_end'];

            $startTime = $start->format('H:i:s');
            $endTime = $end->format('H:i:s');

            $this->hoursStorage->update($day, $startTime, $endTime);
        }


    }

}