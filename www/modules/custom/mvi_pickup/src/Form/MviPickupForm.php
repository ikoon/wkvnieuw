<?php

namespace Drupal\mvi_pickup\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\mvi_pickup\MviPickupResolver;
use Drupal\commerce_store\CurrentStore;
use Drupal\commerce_cart\CartProvider;

/**
 * Class MviPickupForm.
 */
class MviPickupForm extends FormBase {

  /**
   * @var \Drupal\mvi_pickup\MviPickupResolver
   */
  protected $resolver;

  /**
   * @var \Drupal\commerce_store\CurrentStore
   */
  protected $current_store;

  /**
   * @var \Drupal\commerce_cart\CartProvider
   */
  protected $cart_provider;

  /**
   * Constructs a new MviPickupForm object.
   */
  public function __construct(
    MviPickupResolver $mvi_pickup_pickup_resolver,
    CurrentStore $current_store,
    CartProvider $cart_provider
  ) {
    $this->resolver = $mvi_pickup_pickup_resolver;
    $this->currentStore = $current_store;
    $this->cartProvider = $cart_provider;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('mvi_pickup.pickup_resolver'),
      $container->get('commerce_store.current_store'),
      $container->get('commerce_cart.cart_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mvi_pickup_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $cart = $this->resolver->getCart();
    $this->resolver->addPickupTimesToForm($form, $cart);

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update pickup time')
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $errors = $this->resolver->getPickupTimesErrors(
      $form_state->getValue('date'),
      $form_state->getValue('start'),
      $form_state->getValue('end')
    );

    foreach ($errors as $fieldname => $msgs) {
      foreach ($msgs as $msg) {
        $form_state->setErrorByName($fieldname, $msg);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $cart = $this->resolver->getCart();
    $this->resolver->updatePickupTimes(
      $form_state->getValue('date'),
      $form_state->getValue('start'),
      $form_state->getValue('end'),
      $cart
    );
    $messenger = \Drupal::messenger();
    $messenger->addMessage($this->t('The pickup time has been updated.'), $messenger::TYPE_STATUS);
  }

}
