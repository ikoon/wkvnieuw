<?php

namespace Drupal\mvi_pickup\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Url;

use Drupal\state_machine\WorkflowManager;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ExportPiecesForm.
 */
class ExportPiecesForm extends FormBase {

  /**
   * @var WorkflowManager $workflowManager
   */
  protected $workflowManager;

  /**
   * Class constructor.
   * 
   */
  public function __construct(WorkflowManager $workflowManager) {
    $this->workflowManager = $workflowManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('plugin.manager.workflow')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'export_pieces_form';
  }

  /**
   * Gets all states 
   * 
   */
  private function getDistinctOrderStates() {
    $labels = $this->getOrderStateLabels();
    $database = \Drupal::database();
    $query = $database->query("SELECT DISTINCT(state) FROM {commerce_order}");
    $result = $query->fetchAll();
    $states = [];
    foreach ($result as $state) {
      $states[$state->state] = isset($labels[$state->state]) ? $labels[$state->state] : $state->state;
    }
    return $states;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['from'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Pickup time between'),
      '#required' => true,
      '#weight' => '0',
      '#default_value' => DrupalDateTime::createFromTimestamp(strtotime('today midnight'))
    ];
    $form['to'] = [
      '#type' => 'datetime',
      '#title' => $this->t('And'),
      '#required' => true,
      '#weight' => '1',
      '#default_value' => DrupalDateTime::createFromTimestamp(strtotime('tomorrow midnight'))
    ];
    $form['states'] = [
      '#type' => 'select',
      '#multiple' => true,
      '#title' => $this->t('States'),
      '#required' => true,
      '#weight' => '2',
      '#options' => $this->getDistinctOrderStates(),
      '#default_value' => ['fulfillment', 'completed']
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Export pieces'),
      '#weight' => '9',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // foreach ($form_state->getValues() as $key => $value) {
      // @TODO: Validate fields.
    // }
    parent::validateForm($form, $form_state);
  }

  /**
   * Gets all states and labels, possible for orders
   */
  private function getOrderStateLabels() {
    $labels = [];
    $definitions = $this->workflowManager->getDefinitions();
    foreach ($definitions as $name => $arr) {
      if (substr($name, 0, strlen('order_')) === 'order_') {
        foreach ($arr['states'] as $n => $label) {
          if (!isset($labels[$n])) {
            $labels[$n] = $label['label'];
          }
        }
      }
    }
    return $labels;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $from = intval($values['from']->format('U'));
    $to = intval($values['to']->format('U'));
    $states = implode(';;', array_keys($values['states']));

    $url = Url::fromRoute('mvi_pickup.export_pieces_controller_page', [], ['query' => compact('from', 'to', 'states')]);
    $form_state->setRedirectUrl($url);
  }

}
