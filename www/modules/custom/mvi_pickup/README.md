# MVI Commerce Menu README

## Installation
- Enable this module, you will notice the following:
	- 2 new fields for the default Order:
		- field_pickup_from: automatically filled in, can be overwritten without validation at the edit order form (with valid permissions)
		- field_pickup_to: automatically filled in, can be overwritten without validation at the edit order form (with valid permissions)
	- 1 new commerce checkout pane:
		- Pick up order : Put this at the desired step in the checkout flow, this will show the form to set the pickup time for the current cart
	- 1 new Block:
		- Select pickup times block : Place this at the desired region, this will show the form to set the pickup time for the current cart
	- 1 new Taxonomy Vocabulary:
		- Relatieve ophaalmomenten : This represents a time-frame (2 hours for example), create as many as you want.
	- 1 new Field for the default Product : 
		- Relatief minimum ophaalmoment: By default this is not visible on the forms. To activate this feature just add the field in the form display. To add this feature to other product types, just add the same field there.
	- 2 new global (config) forms (but saved in DB, not in Drupal config):
		- Manage opening hours times, at /admin/commerce/pickup/hours
		- Manage Pickup settings, at /admin/commerce/pickup/settings

## Example
- Read the descriptions at the opening hours times thoroughly, set some values, also add some relative pickup times to products and play around with the pickup time as an non-admin user (having a single cart, admins can have multiple carts). You can even try to fiddle around with setting the pickup times at the checkout pane or in the custom block to invalid datetimes/datetimes outside of the openinghours. You should not be able to update the pickup time. Or if you set a pickup time, then wait for this time to have passed, you will not be able to complete or pay the order, but will see clear error messages (if you have enabled Drupal messages).
