(function ($, window) {
  'use strict';
  /**
   * Pickup js
   */
  Drupal.behaviors.pickupOrder = {
    attach: function (context, settings) {
      $(context)
        .find('[data-pickup-order]')
        .not('[data-pickup-order-initted]')
        .each(function () {
          $(this).attr('data-pickup-order-initted', '1');
          var $dateInput = $(this).find('[data-pickup-date]');

          var $endInput = $(this).find('[data-pickup-time-end]').pickatime({
            format: 'HH:i',
            clear: false
          });
          if (!$endInput[0]) return true; // === continue in a for loop

          var endPicker = $endInput.pickatime('picker');
          endPicker.set('disable', true);
          endPicker.set('enable', false);

          var $startInput = $(this)
            .find('[data-pickup-time-start]')
            .pickatime({
              format: 'HH:i',
              clear: false,
              onSet: function (ct) {
                if (typeof ct.select !== 'undefined') {
                  _setAvailableEndTimes($dateInput.val(), $startInput.val(), endPicker, settings);
                }
              },
            });
          if (!$startInput[0]) return true; // === continue in a for loop

          var startPicker = $startInput.pickatime('picker');
          startPicker.set('disable', true);
          startPicker.set('enable', false);

          var closedDays = _returnClosedDays(
            settings.mvi_pickup.openinghours,
            settings.mvi_pickup.exceptions,
            settings.mvi_pickup.earliestTimestamp,
            settings
          );

          var pickADateSettings = {
            min: new Date('yyyy/mm/dd'),
            format: 'yyyy/mm/dd',
            disable: closedDays,
            onSet: function (ct) {
              if (ct.select) {
                // Clear the time inputs
                $endInput.val('');
                $startInput.val('');
                _setAvailableStartTimes(ct.select, startPicker, settings);
              }
            },
          };
          if (settings.mvi_pickup.maximumTimestamp) {
            pickADateSettings.max = new Date(settings.mvi_pickup.maximumTimestamp * 1000);
          }
          $dateInput.pickadate(pickADateSettings);

          // If there is a default date provided.
          if ($dateInput.val()) {
            var dsplit = $dateInput.val().split('/');
            var d = new Date(dsplit[0], dsplit[1] - 1, dsplit[2]);

            _setAvailableStartTimes(d.getTime(), startPicker, settings);
            _setAvailableEndTimes($dateInput.val(), $startInput.val(), endPicker, settings);
          }
        });
    },
  };

  // Transform the timestamp value in the input fields on order admin page.
  Drupal.behaviors.transformTimestamp = {
    attach: function (context, settings) {
      if ($('#views-exposed-form-commerce-orders-page-1').length) {
        var exposedForm = $('#views-exposed-form-commerce-orders-page-1');
        var minValue = exposedForm.find('#edit-field-pickup-from-value-min').attr('value');
        var maxValue = exposedForm.find('#edit-field-pickup-from-value-max').attr('value');
        if (minValue && maxValue) {
          var minDate = new Date(minValue * 1000);
          var minDay = minDate.getDate();
          var minMonth = minDate.getUTCMonth() + 1;
          var minYear = minDate.getFullYear();
          exposedForm
            .find('#edit-field-pickup-from-value-min')
            .val(minYear + '-' + minMonth + '-' + minDay);

          var maxDate = new Date(maxValue * 1000);
          var maxDay = maxDate.getDate();
          var maxMonth = maxDate.getUTCMonth() + 1;
          var maxYear = maxDate.getFullYear();
          exposedForm
            .find('#edit-field-pickup-from-value-max')
            .val(maxYear + '-' + maxMonth + '-' + maxDay);
        }
      }
    },
  };

  function _getAvailableStartTimes(timestamp, settings) {
    var d = new Date(timestamp);
    var dayOfWeek = d.getDay();
    var openingHours = _returnOpeningHours(dayOfWeek, d, settings);

    // openingHours.start is formatted like 'hh:mm:ss' so you can use substring
    var fromHour = parseInt(openingHours.start.substring(0, 2));
    var fromMinute = parseInt(openingHours.start.substring(3, 6));
    var endHour = parseInt(openingHours.end.substring(0, 2));
    var endMinute = parseInt(openingHours.end.substring(3, 6));

    // if the selected date == the date from the earliest timestamp, disable any hours before the earliest timestamp:
    var earliestTimestampMs = settings.mvi_pickup.earliestTimestamp.timestamp * 1000;
    var earliestDate = new Date(earliestTimestampMs);
    if (
      d.getFullYear() === earliestDate.getFullYear() &&
      d.getMonth() === earliestDate.getMonth() &&
      d.getDate() === earliestDate.getDate()
    ) {
      var earliestHours = parseInt(earliestDate.getHours(), 10);
      var earliestMinutes = parseInt(earliestDate.getMinutes(), 10);
      earliestMinutes = earliestMinutes + (30 - (earliestMinutes % 30));
      if (earliestMinutes >= 60) {
        earliestHours++;
        earliestMinutes = 0;
      }
      if (earliestHours > fromHour) {
        fromHour = earliestHours;
        if (fromHour > endHour) {
          fromHour = endHour;
        }
        fromMinute = earliestMinutes;
      }
    }

    return {
      fromHour: fromHour,
      fromMinute: fromMinute,
      endHour: endHour,
      endMinute: endMinute,
    };
  }

  function _setAvailableStartTimes(timestamp, startPicker, settings) {
    var availableStartTimes = _getAvailableStartTimes(timestamp, settings);

    // Reset the timepickers.
    startPicker.set('disable', true);
    startPicker.set('enable', false);

    startPicker.set('disable', [
      {
        from: [availableStartTimes.fromHour, availableStartTimes.fromMinute],
        to: [availableStartTimes.endHour, availableStartTimes.endMinute - 30 - (availableStartTimes.endMinute % 30)],
      },
    ]);
  }

  function _setAvailableEndTimes(datestring, startTime, endPicker, settings) {
    var dsplit = datestring.split('/');
    var d = new Date(dsplit[0], dsplit[1] - 1, dsplit[2]);
    var dayOfWeek = d.getDay();

    var openingHours = _returnOpeningHours(dayOfWeek, d, settings);

    // openingHours.start is formatted like 'hh:mm:ss' so you can use substring
    var fromHour = parseInt(startTime.substring(0, 2));
    var fromMinute = parseInt(startTime.substring(3, 6));
    var endHour = parseInt(openingHours.end.substring(0, 2));
    var endMinute = parseInt(openingHours.end.substring(3, 6));

    // Reset the timepickers.
    endPicker.set('disable', true);
    endPicker.set('enable', false);
    if (fromHour == endHour && fromMinute == endMinute) {
      return
    }

    if (fromMinute > 30) {
      fromHour++;
      fromMinute = 0;
    } else {
      fromMinute += 30;
    }

    endHour = endHour > 23 ? 23 : endHour;
    endHour = endHour < 0 ? 0 : endHour;

    endMinute -= (endMinute % 30);

    endPicker.set('disable', [
      true,
      {
        from: [fromHour, fromMinute],
        to: [endHour, endMinute]
      }
    ]);
  }

  function _returnOpeningHours(dayOfWeek, date, settings) {
    var openingHoursData = settings.mvi_pickup.openinghours;
    var exceptions = settings.mvi_pickup.exceptions;

    var exceptionObj = false;

    $.each(exceptions, function (key, exception) {
      if (date.setHours(0, 0, 0, 0) == new Date(exception.date * 1000).setHours(0, 0, 0, 0)) {
        exceptionObj = exception;
      }
    });

    if (!exceptionObj) {
      if (dayOfWeek == 1) {
        return openingHoursData.monday;
      } else if (dayOfWeek == 2) {
        return openingHoursData.tuesday;
      } else if (dayOfWeek == 3) {
        return openingHoursData.wednesday;
      } else if (dayOfWeek == 4) {
        return openingHoursData.thursday;
      } else if (dayOfWeek == 5) {
        return openingHoursData.friday;
      } else if (dayOfWeek == 6) {
        return openingHoursData.saturday;
      } else if (dayOfWeek == 0) {
        return openingHoursData.sunday;
      }
    } else {
      return exceptionObj;
    }
  }

  function _returnClosedDays(daysOfWeek, exceptions, earliestTimestamp, settings) {
    var days = [];

    if (daysOfWeek.monday.start == daysOfWeek.monday.end) {
      days.push(1);
    }
    if (daysOfWeek.tuesday.start == daysOfWeek.tuesday.end) {
      days.push(2);
    }
    if (daysOfWeek.wednesday.start == daysOfWeek.wednesday.end) {
      days.push(3);
    }
    if (daysOfWeek.thursday.start == daysOfWeek.thursday.end) {
      days.push(4);
    }
    if (daysOfWeek.friday.start == daysOfWeek.friday.end) {
      days.push(5);
    }
    if (daysOfWeek.saturday.start == daysOfWeek.saturday.end) {
      days.push(6);
    }
    if (daysOfWeek.sunday.start == daysOfWeek.sunday.end) {
      days.push(0);
    }

    // Exceptions
    $.each(exceptions, function (key, exception) {
      if (exception.start == exception.end) {
        var date = new Date(exception.date * 1000);
        days.push(date);
      } else {
        // If the exception is an available day, remove the day from the array.
        var openDate = new Date();
        openDate.setTime(exception.date * 1000); // javascript timestamps are in milliseconds
        days.push([
          openDate.getUTCFullYear(),
          openDate.getMonth(),
          openDate.getUTCDate(),
          'inverted',
        ]);
      }
    });

    // disable every day from now to the earliest timestamp:
    if (earliestTimestamp.diffInSeconds > 0) {
      var diffDays = Math.floor(earliestTimestamp.diffInSeconds / 60 / 60 / 24);
      var now = new Date();
      var earliestTimestampMs = settings.mvi_pickup.earliestTimestamp.timestamp * 1000;
      var earliestDate = new Date(earliestTimestampMs);
      if (diffDays > 0) {
        for (var i = 1; i <= diffDays; i++) {
          var dateTimeToCheckMs = now.getTime() + 24 * 60 * 60 * 1000 * i;
          var dateTimeToCheck = new Date(dateTimeToCheckMs);
          var addIt = true;
          
          // if the day we are checking is equal to the day from the earliest timestamp, we need to keep daysOfWeek in mind:
          if (dateToStrWithoutHours(earliestDate) === dateToStrWithoutHours(dateTimeToCheck)) {
            // only add it if no hours can be selected:
            var dayOfWeek = dayIndexToString(dateTimeToCheck.getDay());
            if (daysOfWeek && daysOfWeek[dayOfWeek] && daysOfWeek[dayOfWeek].end) {
              if (daysOfWeek[dayOfWeek].end === daysOfWeek[dayOfWeek].start) { // closed this day!
                addIt = true;
              } else {
                var storeClosesAtTime = daysOfWeek[dayOfWeek].end;
                var storeClosesAtDate = new Date(dateTimeToCheck.getTime());
                storeClosesAtDate.setHours(storeClosesAtTime.split(':')[0]);
                storeClosesAtDate.setMinutes(storeClosesAtTime.split(':')[1]);
                storeClosesAtDate.setSeconds(storeClosesAtTime.split(':')[2]);
                addIt = storeClosesAtDate.getTime() < dateTimeToCheck.getTime();
              }
            }
          }
          
          if (addIt) {
            days.push(dateTimeToCheck);
          }
        }
      }

      // if no hours can be selected for today, then also add now to the disabled days:
      var todayAtMidnight = new Date(Date.parse(dateToStrWithoutHours(now))).getTime();
      var availableStartTimes = _getAvailableStartTimes(todayAtMidnight, settings);
      var earliestOpenTodayMs =
        todayAtMidnight +
        availableStartTimes.fromHour * 60 * 60 * 1000 +
        availableStartTimes.fromMinute * 60 * 1000;
      if (earliestTimestampMs > earliestOpenTodayMs) {
        days.push(now);
      }
    }

    return days;
  }
  
  /**
   * @param {Date} d 
   *
   * @returns {String} the following format: "YYYY-mm-dd" for example: "2020-11-27"
   */
  function dateToStrWithoutHours(d) {
    var month = d.getMonth() + 1;
    var day = d.getDate();
    return d.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day;
  }
  
  /**
   * @param {number} index From 0 (sunday) to 6 (saturday)
   *
   * @returns {String} The day, for example "monday"
   *
   * @throws error if an invalid index is given
   */
  function dayIndexToString(index) {
    switch (index) {
      case 0:
        return 'sunday';
      case 1:
        return 'monday';
      case 2:
        return 'tuesday';
      case 3:
        return 'wednesday';
      case 4:
        return 'thursday';
      case 5:
        return 'friday';
      case 6:
        return 'saturday';
    }
    throw new Error('The index "' + index + '" can not be converted to a day of the week (minimum 0, maximum 6).');
  }
})(jQuery, window);
