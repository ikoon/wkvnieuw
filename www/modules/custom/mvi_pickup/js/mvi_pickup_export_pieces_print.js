(function ($, window) {
  "use strict";
  /**
   * Export pieces print
   */
  Drupal.behaviors.mviPickupExportPiecesPrint = {
    attach: function (context, settings) {
      $(context).find('[data-js-print]').not('[data-js-print-initted]').each(function () {
        $(this).attr('data-js-print-initted', '').on('click', function (e) {
          e.preventDefault();
          window.print();
        });
      });
    }
  };


})(jQuery, window);
