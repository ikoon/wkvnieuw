<?php

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Implements hook_theme().
 */
function mvi_pickup_theme($existing, $type, $theme, $path) {
    return [
      'mvi_pickup_manage' => [
        'variables' => [
          'exceptions' => [],
          'form' => [],
        ],
        'template' => 'manage',
      ],
      'mvi_pickup_export_pieces_print' => [
        'variables' => [
          'items' => [],
          'columns' => [],
          'from' => '',
          'to' => '',
        ],
        'template' => 'export_pieces_print'
      ],
      'select_pickup_times_block' => [
        'variables' => [
          'form' => NULL,
        ],
        'render element' => 'children',
      ],
      'mvi_pickup_add_to_cart_disabled' => [
        'variables' => [
          'time_order_allowed' => NULL,
        ],
        'template' => 'add-to-cart-disabled'
      ],
    ];
}

/**
 * Implements hook_form_alter().
 */
function mvi_pickup_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  if($form['#id'] == 'views-exposed-form-commerce-orders-page-1'){

    // Change the input type so a html 5 date popup appears.
    $form['field_pickup_from_value']['min']['#type'] = 'date';
    $form['field_pickup_from_value']['max']['#type'] = 'date';
    $form['#attached']['library'][] = 'mvi_pickup/mvi_pickup_pickadate';
  }

  /*
  // Add validation to the add to cart form based on the relative pickup time on the product.
  if (
    substr($form_id, 0, strlen('commerce_order_item_add_to_cart_form_commerce_product_')) === 'commerce_order_item_add_to_cart_form_commerce_product_' &&
    isset($form['purchased_entity']['widget']['0']['variation']['#value'])
  ) {
    $form['#cache']['max-age'] = 0;

    $productVariationId = $form['purchased_entity']['widget']['0']['variation']['#value'];
    $productVariation = ProductVariation::load($productVariationId);
    $product = $productVariation->getProduct();
    $resolver = \Drupal::service('mvi_pickup.pickup_resolver');
    $timestamp = $resolver->canProductBeOrdered($product);
    if ($timestamp !== true) {
      $form['#disabled'] = true;
      $renderer = \Drupal::service('renderer');
      $result = [
        '#theme' => 'mvi_pickup_add_to_cart_disabled',
        '#time_order_allowed' => DrupalDateTime::createFromTimestamp($timestamp->getTimeStamp()),
      ];
      $html = $renderer->render($result);
      $form['#suffix'] = $html;
    }
  }*/

  // if next step is 'payment' or 'complete' for the checkout flow, and the pickup time is incorrect, show the drupal error message & disable the submit button
  if (
    $form_id === 'commerce_checkout_flow_multistep_default' &&
    $order = \Drupal::routeMatch()->getParameter('commerce_order')
  ) {
    $checkoutOrderManager = \Drupal::service('commerce_checkout.checkout_order_manager');

    /** @var \Drupal\commerce_checkout\Entity\CheckoutFlowInterface $checkout_flow */
    $checkoutFlow = $checkoutOrderManager->getCheckoutFlow($order);

    /** @var \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface $checkout_flow_plugin */
    $checkout_flow_plugin = $checkoutFlow->getPlugin();

    /** @var array $steps */
    $steps = $checkout_flow_plugin->getVisibleSteps();
    $stepName = $form['#step_id'];
    if ($stepName && isset($steps[$stepName]) && $steps[$stepName]) {
      $stepNames = array_keys($steps);
      $stepIndex = array_search($stepName, $stepNames);
      if (isset($stepNames[$stepIndex + 1]) && $nextStepName = $stepNames[$stepIndex + 1]) {
        if (
          in_array($nextStepName, ['payment', 'complete']) &&
          !\Drupal::service('mvi_pickup.pickup_resolver')->checkPickupTimeStillGood($order)
        ) {
          $form['actions']['#disabled'] = true;
        }
      }
      // kint($currentStep);
    }
  }
}

/**
 * Implements hook_views_pre_view().
 *
 * Convert the date input string to timestamp so correct filtering can be done.
 */
function mvi_pickup_views_pre_view(\Drupal\views\ViewExecutable $view, $display_id, array &$args) {
  switch ($view->id()) {
    case 'commerce_orders':
      // Get exposed filters value
      $filter_input = $view->getExposedInput();
      if(array_key_exists('field_pickup_from_value', $filter_input)){
        $min = $filter_input['field_pickup_from_value']['min'];
        $max = $filter_input['field_pickup_from_value']['max'];

        if($min && $max){
          if(is_string($min) && is_string($max)) {
            // Set exposed filter value to new one
            $minDate = new DateTime($min);
            $maxDate = new DateTime($max);
            $filter_input['field_pickup_from_value'] = ['min' => $minDate->getTimestamp(), 'max' => $maxDate->getTimestamp()];
            $view->setExposedInput($filter_input);
          }
        }
      }

      break;
  }
}
