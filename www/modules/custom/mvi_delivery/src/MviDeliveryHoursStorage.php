<?php

namespace Drupal\mvi_delivery;

use Drupal\Core\Database\Connection;

/**
 * Class MviPickupHoursStorage
 *
 * @package Drupal\mvi_delivery
 */
class MviDeliveryHoursStorage implements MviDeliveryHoursStorageInterface {

  protected $database;

  /**
   * @param \Drupal\Core\Database\Connection $connection
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  public function update($day, $start = null, $end = null)
  {
    $this->database->update('mvi_delivery_deliveryhours')
      ->fields(['start' => $start, 'end' => $end])
      ->condition('day', $day)
      ->execute();
  }

  public function select($day)
  {
    $query = $this->database->select('mvi_delivery_deliveryhours', 'hours');
    $query->fields('hours', ['start', 'end']);
    $query->condition('day', $day);
    return $query->execute()->fetch();
  }

  public function selectAll()
  {
    $query = $this->database->select('mvi_delivery_deliveryhours', 'hours');
    $query->fields('hours', ['start', 'end', 'day']);
    return $query->execute()->fetchAllAssoc('day');
  }

  public function delete($id)
  {
    $this->database->delete('mvi_delivery_deliveryhours')
      ->condition('id', $id)
      ->execute();
  }
}
