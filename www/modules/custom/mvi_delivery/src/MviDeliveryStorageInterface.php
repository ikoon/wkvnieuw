<?php

namespace Drupal\mvi_delivery;

/**
 * Interface MviDeliveryStorageInterface
 *
 * @package Drupal\mvi_delivery
 */
interface MviDeliveryStorageInterface {
  /**
   * @param $id
   * @param $uid
   * @return mixed
   */
  public function add($date, $start, $end);

  /**
   * @return mixed
   */
  public function select();

  /**
   * @param $id
   * @param $uid
   * @return mixed
   */
  public function delete($id);
}
