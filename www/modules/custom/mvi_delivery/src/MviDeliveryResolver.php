<?php

namespace Drupal\mvi_delivery;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Session\AccountProxy;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_store\CurrentStore;
use Drupal\commerce_cart\CartProvider;

use Drupal\mvi_delivery\MviDeliveryStorageInterface;
use Drupal\mvi_delivery\MviDeliveryHoursStorageInterface;
use Drupal\mvi_delivery\MviDeliveryRelativeMinPickupTimeStorage;

/**
 * Class MviDeliveryResolver.
 */
class MviDeliveryResolver
{
  use StringTranslationTrait;

  /**
   * @var \Drupal\mvi_delivery\MviDeliveryStorageInterface
   */
  protected $exceptionStorage;

  /**
   * @var \Drupal\mvi_delivery\MviDeliveryHoursStorageInterface
   */
  protected $hoursStorage;

  /**
   * @var \Drupal\mvi_delivery\MviDeliveryRelativeMinPickupTimeStorage
   */
  protected $timingStorage;

  /**
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * @var \Drupal\commerce_store\CurrentStore
   */
  protected $current_store;

  /**
   * @var \Drupal\commerce_cart\CartProvider
   */
  protected $cart_provider;

  /**
   * Constructs a new MviDeliveryResolver object.
   */
  public function __construct(
    MviDeliveryStorageInterface $mvi_delivery_storage,
    MviDeliveryHoursStorageInterface $mvi_delivery_hours_storage,
    MviDeliveryRelativeMinPickupTimeStorage $mvi_delivery_rel_min_pickup_time_storage,
    AccountProxy $current_user,
    CurrentStore $current_store,
    CartProvider $cart_provider
  ) {
    $this->exceptionStorage = $mvi_delivery_storage;
    $this->hoursStorage = $mvi_delivery_hours_storage;
    $this->timingStorage = $mvi_delivery_rel_min_pickup_time_storage;
    $this->currentUser = $current_user;
    $this->currentStore = $current_store;
    $this->cartProvider = $cart_provider;
  }

  /**
   * @param \Drupal\commerce_product\Entity\Product $product
   *
   * @return true|\TimeStamp true if can be ordered, the timestamp from when it can be ordered if can not be ordered
   */
  public function canProductBeOrdered(Product $product, Order $order = null){
    $relativeTimestamp = $this->timingStorage->getRelativeTimestampFromProduct($product);
    if (is_null($relativeTimestamp)) {
      return true;
    }

    if (is_null($order)) {
      $order = $this->getCart();
    }
    $currentStartDateTime = $this->getDeliveryStartDate($order);
    if (is_null($currentStartDateTime)) {
      return true;
    }

    $relativeDate = date('Y/m/d H:i', $relativeTimestamp);
    $relativeDateTime = \DateTime::createFromFormat('Y/m/d H:i', $relativeDate);

    if ($relativeDateTime->getTimestamp() < $currentStartDateTime->getTimestamp()) {
      return true;
    }
    return $relativeDateTime;
  }

  /**
   * @return \Drupal\commerce_order\Entity\Order the current cart of type 'default' (or a new one if the user did not have one already)
   */
  public function getCart() {
    $store = $this->currentStore->getStore();
    $cart = $this->cartProvider->getCart('default', $store);
    if (!$cart) {
      $cart = $this->cartProvider->createCart('default', $store);
    }
    return $cart;
  }

  /**
   * @param \Drupal\commerce_order\Entity\Order $order
   *
   * @return null|\DateTime null if none was found, a PHP DateTime object otherwise
   */
  private function getDeliveryStartDate(Order $order) {
    if (!$order->hasField('field_preferred_delivery')) {
      return null;
    }

    if ($order->get('field_preferred_delivery')->isEmpty()) {
      return null;
    }

    $val = $order->get('field_preferred_delivery')->first()->getValue();
    if ($from = $val['value']) {
      // If the date is before now, reset the date to now. Otherwise, the date delivery calendar wil set a date in
      // the past as default value.
      $now = date('Y/m/d H:i');
      $strDate = date('Y/m/d H:i', $from);
      if($strDate < $now) {
        $strDate = $now;
      }
      return \DateTime::createFromFormat('Y/m/d H:i', $strDate);
    }

    return null;
  }

  /**
   * Adds the date fields (& the JS-libaries) to the given form
   *
   * @param array $form
   * @param \Drupal\commerce_order\Entity\Order $order
   *
   * @return array The updated form
   */
  public function addDeliveryTimesToForm(array &$form, Order $order)
  {
    $currentStartTime = $this->getDeliveryStartDate($order);

    $form['deliver_order'] = [
      '#type' => 'container',
      '#attributes' => ['data-deliver-order' => '1'],
    ];

    $form['deliver_order']['date'] = [
      '#title' => $this->t('Date'),
      '#type' => 'textfield',
      '#placeholder' => $this->t('YYYY/mm/dd'),
      '#attributes' => ['data-deliver-date' => '1'],
    ];

    if (!is_null($currentStartTime)) {
      $this->checkDeliveryTimeStillGood($order);
    }
    if (!is_null($currentStartTime)) {
      $form['deliver_order']['date']['#default_value'] = $currentStartTime->format('Y/m/d');
    }

    $openingHours = $this->hoursStorage->selectAll();
    $exceptions = $this->exceptionStorage->select();
    $earliestTimestamp = $this->timingStorage->selectEarliestTimestamp($order);

    $form['#cache']['max-age'] = 0;
    $form['#attached']['library'][] = 'mvi_delivery/mvi_delivery_checkout';
    $form['#attached']['library'][] = 'mvi_delivery/mvi_pickup_pickadate';
    $form['#attached']['drupalSettings']['mvi_delivery']['openinghours'] = $openingHours;
    $form['#attached']['drupalSettings']['mvi_delivery']['exceptions'] = $exceptions;
    $form['#attached']['drupalSettings']['mvi_delivery']['earliestTimestamp'] = $earliestTimestamp;

    return $form;
  }

  /**
   * @param string $strDate as 'Y/m/d' - example: '2020/08/20'
   * @param Order $order
   *
   * @return null|array null if no values are given/no errors occurred.Or an associative array with 1 key per field ('date') and the value is an array of error messages.
   */
  public function getDeliveryTimesErrors($strDate, $order = null) {
    if (!($strDate)) {
      return null;
    }

    $date = \DateTime::createFromFormat('Y/m/d', $strDate);

    $result = [];
    if (!$date) {
      $result['date'][] = $this->t('Invalid date date "@date" given, please provide a date in the form YYYY/mm/dd.', ['@date' => $strDate]);
    }

    if (count($result) > 0) {
      return $result;
    }

    $fromTimestamp = $date->getTimestamp();

    // check if $min is equal or less to earliest timestamp
    $min = $this->timingStorage->selectEarliestTimestamp($order);
    if ($min && isset($min['timestamp']) && $fromTimestamp < $min['timestamp']) {
      $result['date'][] = $this->t('The start date "@from" is invalid.', ['@from' => $date->format('Y/m/d H:i')]);
      return $result;
    }

    return count($result) > 0 ? $result : null;
  }

  /**
   * @param string $strDate as 'Y/m/d' - example: '2020/08/20'
   * @param \Drupal\commerce_order\Entity\Order $order
   */
  public function updateDeliverTimes($strDate, Order $order) {
    $errors = $this->getDeliveryTimesErrors($strDate);
    if (!is_null($errors)) {
      throw new \Exception('Can not save invalid data. Date: ' . $strDate);
    }

    if($strDate) {
      $from = \DateTime::createFromFormat('Y/m/d', $strDate);

      $order->set('field_preferred_delivery', $from->getTimestamp());
    } else {
      $order->set('field_preferred_delivery', null);
    }

    $order->save();
  }

  /**
   * Shows a drupal message when one or more products in the shopping cart can not be picked up at the chosen time
   *
   * @param \Drupal\commerce_order\Entity\Order $order
   *
   * @return boolean true it the given delivery time is valid (or if there is none), false if not
   */
  public function checkDeliveryTimeStillGood(Order $order) {
    if (!$order) return true;

    $result = true;

    $orderItems = $order->getItems();
    foreach ($orderItems as $orderItem) {
      $product = $orderItem->getPurchasedEntity()->getProduct();
      $timestamp = $this->canProductBeOrdered($product);
      if ($timestamp !== true) {
        \Drupal::messenger()->addError(t('The product "@product" can not be ordered untill @datetime. Please remove the product from the cart or update the preferred delivery time to be able to complete your order.', [
          '@product' => $orderItem->getTitle(),
          '@datetime' => $timestamp->format('Y/m/d H:i'),
        ]));
        $result = false;
      }
    }
    return $result;
  }
}
