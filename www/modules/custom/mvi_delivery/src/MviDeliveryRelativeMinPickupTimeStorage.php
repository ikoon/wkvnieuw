<?php

namespace Drupal\mvi_delivery;

use Drupal\commerce_store\CurrentStore;
use Drupal\commerce_cart\CartProvider;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Session\AccountProxy;
use Drupal\taxonomy\Entity\Term;

use Drupal\mvi_delivery\MviDeliveryHoursStorage;
use Drupal\mvi_delivery\MviDeliveryStorage;

/**
 * Class MviDeliveryRelativeMinPickupTimeStorage
 *
 * @package Drupal\mvi_delivery
 */
class MviDeliveryRelativeMinPickupTimeStorage {

  protected $currentStore;
  protected $cartProvider;
  protected $currentUser;
  protected $storage;
  protected $hoursStorage;

  /**
   * @param \Drupal\commerce_store\CurrentStore $currentStore
   * @param \Drupal\commerce_cart\CartProvider $cartProvider
   * @param \Drupal\Core\Session\AccountProxy $currentUser
   * @param \Drupal\mvi_delivery\MviDeliveryStorage $storage
   * @param \Drupal\mvi_delivery\MviDeliveryHoursStorage $hours_storage
   */
  public function __construct(CurrentStore $currentStore, CartProvider $cartProvider, AccountProxy $currentUser, MviDeliveryStorage $storage, MviDeliveryHoursStorage $hours_storage) {
      $this->currentStore = $currentStore;
      $this->cartProvider = $cartProvider;
      $this->currentUser = $currentUser;
      $this->storage = $storage;
      $this->hoursStorage = $hours_storage;
  }

  /**
   * @param \Drupal\commerce_product\Entity\Product $product
   *
   * @return null|int A timestamp if a relative pickup time is set or null if not
   */
  public function getRelativeTimestampFromProduct(Product $product) {
    if (
      $product->hasField('field_relative_minimum_pickup_ti') &&
      $relMinPickupTime = $product->get('field_relative_minimum_pickup_ti')->first()
    ) {
      $term = $relMinPickupTime->get('entity')->getTarget()->getValue();
      return $this->getSecondsFromRelativeTime($term) + time();
    }
    return null;
  }

  public function selectEarliestTimestamp(Order $order = null) {
    $earliestTimestamp = $this->getEarliestTimestampFromConfig();

    // get earliest timestamp from products relative minimum pickup times
    if (!is_null($order)) {
      $orderItems = $order->getItems();
      foreach ($orderItems as $orderItem) {
        if ($product = $orderItem->getPurchasedEntity()->getProduct()) {
          $timestamp = $this->getRelativeTimestampFromProduct($product);
          if ($timestamp > $earliestTimestamp) {
            $earliestTimestamp = $timestamp;
          }
        }
      }
    }

    // also, check the opening hours,
    $dayOfEarliestMoment = strtolower(date('l', $earliestTimestamp));
    $openingHours = $this->hoursStorage->select($dayOfEarliestMoment);
    // if the earliest moment is 02:00:00 on a wednesday and wednesday is open from 10:00:00 to 18:00:00,
    $openingHoursMin = strtotime(date('Y-m-d', $earliestTimestamp) . ' ' . $openingHours->start);
    // then the earliest moment should be the day of the earliest moment (wednesday) at 10:00:00
    if ($openingHoursMin > $earliestTimestamp) {
      $earliestTimestamp = $openingHoursMin;
    }
    // also, if the earliest moment is 20:00:00 on a wednesday and wednesday is open from 10:00:00 to 18:00:00,
    $openingHoursMax = strtotime(date('Y-m-d', $earliestTimestamp) . ' ' . $openingHours->end);
    if ($earliestTimestamp > $openingHoursMax) {
      // not today, but the next day in opening hours:
      $days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saterday', 'sunday'];
      $daysIndex = array_search($dayOfEarliestMoment, $days);
      if ($daysIndex !== false) {
        $originalDaysIndex = $daysIndex;
        $allOpeningHours = $this->hoursStorage->selectAll();
        $nextStartTime = false;
        while($nextStartTime === false) {
          $daysIndex++;
          if ($daysIndex > (count($days) - 1)) {
            $daysIndex = 0;
          }
          if ($daysIndex === $originalDaysIndex) {
            break;
          }

          $openingHours = $allOpeningHours[$days[$daysIndex]];
          if ($openingHours->start === '00:00:00' && $openingHours->end === '00:00:00') {
            continue;
          }
          $nextStartTime = strtotime('next ' . $openingHours->day . ' ' . $openingHours->start);
        }
        if ($nextStartTime !== false && $nextStartTime > $earliestTimestamp) {
          $earliestTimestamp = $nextStartTime;
        }
      }
    }
    // and on thursdays also from 10:00:00 to 18:00:00, then the earliest moment will be thursday at 10:00:00
    return [
      'timestamp' => $earliestTimestamp,
      'diffInSeconds' => $earliestTimestamp - time()
    ];
  }

  /**
   * @param \Drupal\taxonomy\Entity\Term $term
   *
   * @return null|int amount of seconds as integer or null if non if term is not a valid term (needs fields 'field_quantity' & 'field_time_unit')
   */
  private function getSecondsFromRelativeTime(Term $term) {
    if (
      $term->hasField('field_quantity') &&
      !$term->get('field_time_unit')->isEmpty() &&
      $term->hasField('field_time_unit') &&
      !$term->get('field_time_unit')->isEmpty()
    ) {
      $quantity = $term->get('field_quantity')->first()->getValue();
      $quantity = intval($quantity['value']);
      $timeUnit = $term->get('field_time_unit')->first()->getValue();
      $timeUnit = $timeUnit['value'];
      return strtotime("+$quantity $timeUnit", 0);
    }

    return null;
  }

  /**
   * calculates the earliest timestamp a user can order depending on the global config (now + amount of seconds found in config)
   *
   * @return int timestemp
   */
  private function getEarliestTimestampFromConfig() {
    $min = $this->storage->getStaticDateTime('min');
    $relativeMin = $this->storage->getStaticDateTime('relative_min');
    $result = time();
    if (is_null($min) && is_null($relativeMin)) {
      return $result;
    }

    if (!is_null($min) && $min > $result) {
      $result = $min;
    }

    if (!is_null($relativeMin) && $term = Term::load($relativeMin)) {
      $seconds = $this->getSecondsFromRelativeTime($term);
      $timestamp = time() + $seconds;
      if ($timestamp > $result) {
        $result = $timestamp;
      }
    }

    return $result;
  }

  /**
   * calculates the maximum timestamp a user can order depending on the global config
   *
   * @return int|null timestamp or null if no max is set
   */
  public function selectMaximumTimestamp() {
    return $this->storage->getStaticDateTime('max');
  }
}
