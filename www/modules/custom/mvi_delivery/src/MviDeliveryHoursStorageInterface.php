<?php

namespace Drupal\mvi_delivery;

/**
 * Interface MviDeliveryHoursStorageInterface
 *
 * @package Drupal\mvi_delivery
 */
interface MviDeliveryHoursStorageInterface {

  /**
   * @param $day
   * @param $start
   * @param $end
   * @return mixed
   */
  public function update($day, $start = null, $end = null);

  /**
   * @param $day
   * @return mixed
   */
  public function select($day);

  /**
   * @return array
   */
  public function selectAll();

  /**
   * @param $day
   * @return mixed
   */
  public function delete($day);
}
