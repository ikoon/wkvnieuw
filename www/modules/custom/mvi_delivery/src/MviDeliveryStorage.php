<?php

namespace Drupal\mvi_delivery;

use Drupal\Core\Database\Connection;

/**
 * Class MviDeliveryStorage
 *
 * @package Drupal\mvi_delivery
 */
class MviDeliveryStorage implements MviDeliveryStorageInterface {

  protected $database;

  /**
   * @param \Drupal\Core\Database\Connection $connection
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  public function add($date, $start, $end)
  {
    $this->database->insert('mvi_delivery_exceptions')
      ->fields(['date' => $date, 'start' => $start, 'end' => $end])
      ->execute();
  }

  public function select()
  {
    $query = $this->database->select('mvi_delivery_exceptions', 'exceptions');
    $query->orderBy('date', 'asc');
    $query->fields('exceptions');
    return $query->execute()->fetchAll();
  }

  public function delete($id)
  {
    $this->database->delete('mvi_delivery_exceptions')
      ->condition('id', $id)
      ->execute();
  }

  /**
   * @param string $key
   *
   * @return null|int null if not found, timestamp as integer if found
   */
  public function selectStaticDateTime($key) {
    return $this->database->select('mvi_delivery_static_datetimes')
      ->fields('mvi_delivery_static_datetimes', ['value'])
      ->condition('mvi_delivery_static_datetimes.key', $key)
      ->execute()
      ->fetchAssoc();
  }

  /**
   * @param string $key
   *
   * @return null|int null if not found, timestamp as integer if found
   */
  public function getStaticDateTime($key) {
    $result = $this->database->select('mvi_delivery_static_datetimes')
      ->fields('mvi_delivery_static_datetimes', ['value'])
      ->condition('mvi_delivery_static_datetimes.key', $key)
      ->execute()
      ->fetchAssoc();
    if (!$result || !isset($result['value'])) {
      return null;
    }

    return intval($result['value']);
  }

  /**
   * @param string $key
   * @param int $value
   *
   * @return int the id when inserted properly
   */
  public function setStaticDateTime($key, $value) {
    if (!(is_int($value) || is_null($value))) {
      kint(gettype($value));
      throw new \Exception('MVI delivery static datetimes accept only integers or NULL as value!');
    }

    // check if key is already there:
    $result = $this->selectStaticDateTime($key);
    if (!$result) {
      return $this->database->insert('mvi_delivery_static_datetimes')
        ->fields(['key' => $key, 'value' => $value])
        ->execute();
    }

    return $this->database->update('mvi_delivery_static_datetimes')
      ->condition('mvi_delivery_static_datetimes.key', $key)
      ->fields(['value' => $value])
      ->execute();
  }
}
