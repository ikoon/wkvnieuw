<?php

namespace Drupal\mvi_delivery\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\mvi_delivery\MviDeliveryHoursStorage;
use Drupal\mvi_delivery\MviDeliveryRelativeMinPickupTimeStorage;
use Drupal\mvi_delivery\MviDeliveryStorage;
use Drupal\mvi_delivery\MviDeliveryResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a checkout pane where the customer can select his preferred delivery date.
 *
 * @CommerceCheckoutPane(
 *   id = "preferred_delivery",
 *   label = @Translation("Preferred delivery date"),
 *   wrapper_element = "fieldset",
 * )
 */
class PreferredDelivery extends CheckoutPaneBase implements ContainerFactoryPluginInterface
{

  /**
   * Constructs a new CheckoutPaneBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface $checkout_flow
   *   The parent checkout flow.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\mvi_delivery\MviDeliveryRelativeMinPickupTimeStorage $timing_storage
   *   The pickup timing storage
   * @param \Drupal\mvi_delivery\MviDeliveryHoursStorage $hours_storage
   *   The opening hours storage
   * @param \Drupal\mvi_delivery\MviDeliveryStorage $exception_storage
   *   The exceptions storage
   * @param \Drupal\mvi_delivery\MviDeliveryResolver $resolver
   *   The pickup resolver
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow, EntityTypeManagerInterface $entity_type_manager, MviDeliveryRelativeMinPickupTimeStorage $timing_storage, MviDeliveryHoursStorage $hours_storage, MviDeliveryStorage $exception_storage, MviDeliveryResolver $resolver)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $checkout_flow, $entity_type_manager);

    $this->checkoutFlow = $checkout_flow;
    $this->order = $checkout_flow->getOrder();
    $this->setConfiguration($configuration);

    $this->entityTypeManager = $entity_type_manager;
    $this->timingStorage = $timing_storage;
    $this->hoursStorage = $hours_storage;
    $this->exceptionStorage = $exception_storage;
    $this->resolver = $resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $checkout_flow,
      $container->get('entity_type.manager'),
      $container->get('mvi_delivery.rel_min_pickup_time_storage'),
      $container->get('mvi_delivery.hours_storage'),
      $container->get('mvi_delivery.storage'),
      $container->get('mvi_delivery.delivery_resolver')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneSummary() {
    $summary = null;

    $date = $this->order->get('field_preferred_delivery')->getValue();
    if($date) {
      $summary = $this->t("Levering op:") . " <strong>" . date('d/m/Y', $date[0]['value']) . "</strong>";
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form)
  {
    $this->resolver->addDeliveryTimesToForm($pane_form, $this->order);
    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function validatePaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {

    $shippingInformation = $form_state->getValue('mvi_shipping_information');
    $shippingMethod = $shippingInformation["shipments"][0]["shipping_method"][0];
    // When the pickup order shipping method is selected, require the stard and end fields.
    if($shippingMethod == '1--default') {
      $pickupData = $form_state->getValue('preferred_delivery');

      if(!$pickupData['deliver_order']['date']) {
        $form_state->setErrorByName('deliver_order', 'Gelieve een gewenste leverdatum op te geven.');

      } else if($pickupData) {
        $errors = $this->resolver->getDeliveryTimesErrors(
          $pickupData['deliver_order']['date']
        );

        if ($errors) {
          foreach($errors as $messages) {
            foreach($messages as $msg) {
              $form_state->setErrorByName('deliver_order', $msg);
            }
          }
        }
      }
    }

  }

  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $shippingInformation = $form_state->getValue('mvi_shipping_information');
    $shippingMethod = $shippingInformation["shipments"][0]["shipping_method"][0];
    // When the pickup order shipping method is selected, require the stard and end fields.
    if($shippingMethod == '1--default') {
      $pickupData = $form_state->getValue('preferred_delivery');

      $this->resolver->updateDeliverTimes(
        $pickupData['deliver_order']['date'],
        $this->order
      );
    } else {
      $this->resolver->updateDeliverTimes(
        null,
        $this->order
      );
    }
  }

}
