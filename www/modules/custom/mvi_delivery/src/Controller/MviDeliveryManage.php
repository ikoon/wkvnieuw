<?php

namespace Drupal\mvi_delivery\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\mvi_delivery\MviDeliveryStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MviDeliveryManage extends ControllerBase{

  protected $storage;

  public function __construct(MviDeliveryStorage $storage) {
    $this->storage = $storage;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('mvi_delivery.storage')
    );
  }

  public function managePage(){

    $form = \Drupal::formBuilder()->getForm('Drupal\mvi_delivery\Form\DeliveryManageSettingsForm');

    $build = [
      '#theme' => 'mvi_delivery_manage',
      '#form' => $form
    ];

    return $build;
  }
}
