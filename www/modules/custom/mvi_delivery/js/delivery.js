(function ($, window) {
  'use strict';
  /**
   * Pickup js
   */
  Drupal.behaviors.deliverOrder = {
    attach: function (context, settings) {
      $(context)
        .find('[data-deliver-order]')
        .not('[data-deliver-order-initted]')
        .each(function () {
          $(this).attr('data-deliver-order-initted', '1');
          var $dateInput = $(this).find('[data-deliver-date]');

          var closedDays = _returnClosedDays(
            settings.mvi_delivery.openinghours,
            settings.mvi_delivery.exceptions,
            settings.mvi_delivery.earliestTimestamp,
            settings
          );

          var pickADateSettings = {
            min: new Date('yyyy/mm/dd'),
            format: 'yyyy/mm/dd',
            disable: closedDays,
          };
          if (settings.mvi_delivery.maximumTimestamp) {
            pickADateSettings.max = new Date(settings.mvi_delivery.maximumTimestamp * 1000);
          }
          $dateInput.pickadate(pickADateSettings);
        });
    },
  };

  // Transform the timestamp value in the input fields on order admin page.
  Drupal.behaviors.transformTimestamp = {
    attach: function (context, settings) {
      if ($('#views-exposed-form-commerce-orders-page-1').length) {
        var exposedForm = $('#views-exposed-form-commerce-orders-page-1');
        var minValue = exposedForm.find('#edit-field-preferred-delivery-value-min').attr('value');
        var maxValue = exposedForm.find('#edit-field-preferred-delivery-value-max').attr('value');

        if (minValue && maxValue) {
          var minDate = new Date(minValue * 1000);
          var minDay = minDate.getDate();
          var minMonth = minDate.getUTCMonth() + 1;
          var minYear = minDate.getFullYear();

          exposedForm
            .find('#edit-field-preferred-delivery-value-min')
            .val(minYear + '-' + minMonth + '-' + minDay);

          var maxDate = new Date(maxValue * 1000);
          var maxDay = maxDate.getDate();
          var maxMonth = maxDate.getUTCMonth() + 1;
          var maxYear = maxDate.getFullYear();
          exposedForm
            .find('#edit-field-preferred-delivery-value-max')
            .val(maxYear + '-' + maxMonth + '-' + maxDay);
        }
      }
    },
  };

  function _getAvailableStartTimes(timestamp, settings) {
    var d = new Date(timestamp);
    var dayOfWeek = d.getDay();
    var openingHours = _returnOpeningHours(dayOfWeek, d, settings);

    // openingHours.start is formatted like 'hh:mm:ss' so you can use substring
    var fromHour = parseInt(openingHours.start.substring(0, 2));
    var fromMinute = parseInt(openingHours.start.substring(3, 6));
    var endHour = parseInt(openingHours.end.substring(0, 2));
    var endMinute = parseInt(openingHours.end.substring(3, 6));

    // if the selected date == the date from the earliest timestamp, disable any hours before the earliest timestamp:
    var earliestTimestampMs = settings.mvi_delivery.earliestTimestamp.timestamp * 1000;
    var earliestDate = new Date(earliestTimestampMs);
    if (
      d.getFullYear() === earliestDate.getFullYear() &&
      d.getMonth() === earliestDate.getMonth() &&
      d.getDate() === earliestDate.getDate()
    ) {
      var earliestHours = parseInt(earliestDate.getHours(), 10);
      var earliestMinutes = parseInt(earliestDate.getMinutes(), 10);
      earliestMinutes = earliestMinutes + (30 - (earliestMinutes % 30));
      if (earliestMinutes >= 60) {
        earliestHours++;
        earliestMinutes = 0;
      }
      if (earliestHours > fromHour) {
        fromHour = earliestHours;
        if (fromHour > endHour) {
          fromHour = endHour;
        }
        fromMinute = earliestMinutes;
      }
    }

    return {
      fromHour: fromHour,
      fromMinute: fromMinute,
      endHour: endHour,
      endMinute: endMinute,
    };
  }

  function _returnOpeningHours(dayOfWeek, date, settings) {
    var openingHoursData = settings.mvi_delivery.openinghours;
    var exceptions = settings.mvi_delivery.exceptions;

    var exceptionObj = false;

    $.each(exceptions, function (key, exception) {
      if (date.setHours(0, 0, 0, 0) == new Date(exception.date * 1000).setHours(0, 0, 0, 0)) {
        exceptionObj = exception;
      }
    });

    if (!exceptionObj) {
      if (dayOfWeek == 1) {
        return openingHoursData.monday;
      } else if (dayOfWeek == 2) {
        return openingHoursData.tuesday;
      } else if (dayOfWeek == 3) {
        return openingHoursData.wednesday;
      } else if (dayOfWeek == 4) {
        return openingHoursData.thursday;
      } else if (dayOfWeek == 5) {
        return openingHoursData.friday;
      } else if (dayOfWeek == 6) {
        return openingHoursData.saturday;
      } else if (dayOfWeek == 0) {
        return openingHoursData.sunday;
      }
    } else {
      return exceptionObj;
    }
  }

  function _returnClosedDays(daysOfWeek, exceptions, earliestTimestamp, settings) {
    var days = [];

    if (daysOfWeek.monday.start == daysOfWeek.monday.end) {
      days.push(1);
    }
    if (daysOfWeek.tuesday.start == daysOfWeek.tuesday.end) {
      days.push(2);
    }
    if (daysOfWeek.wednesday.start == daysOfWeek.wednesday.end) {
      days.push(3);
    }
    if (daysOfWeek.thursday.start == daysOfWeek.thursday.end) {
      days.push(4);
    }
    if (daysOfWeek.friday.start == daysOfWeek.friday.end) {
      days.push(5);
    }
    if (daysOfWeek.saturday.start == daysOfWeek.saturday.end) {
      days.push(6);
    }
    if (daysOfWeek.sunday.start == daysOfWeek.sunday.end) {
      days.push(0);
    }

    // Exceptions
    $.each(exceptions, function (key, exception) {
      if (exception.start == exception.end) {
        var date = new Date(exception.date * 1000);
        days.push(date);
      } else {
        // If the exception is an available day, remove the day from the array.
        var openDate = new Date();
        openDate.setTime(exception.date * 1000); // javascript timestamps are in milliseconds
        days.push([
          openDate.getUTCFullYear(),
          openDate.getMonth(),
          openDate.getUTCDate(),
          'inverted',
        ]);
      }
    });

    // disable every day from now to the earliest timestamp:
    if (earliestTimestamp.diffInSeconds > 0) {
      var diffDays = Math.floor(earliestTimestamp.diffInSeconds / 60 / 60 / 24);
      var today = new Date();
      if (diffDays > 0) {
        for (var i = 1; i <= diffDays; i++) {
          days.push(new Date(today.getTime() + 24 * 60 * 60 * 1000 * i));
        }
      }

      // if no hours can be selected for today, then also add today to the disabled days:
      var month = today.getMonth() + 1;
      var day = today.getDate();
      var todayAtMidnight = new Date(
        today.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day
      ).getTime();
      var availableStartTimes = _getAvailableStartTimes(todayAtMidnight, settings);
      var earliestOpenTodayMs =
        todayAtMidnight +
        availableStartTimes.fromHour * 60 * 60 * 1000 +
        availableStartTimes.fromMinute * 60 * 1000;
      var earliestTimestampMs = settings.mvi_delivery.earliestTimestamp.timestamp * 1000;
      if (earliestTimestampMs > earliestOpenTodayMs) {
        days.push(today);
      }
    }

    return days;
  }
})(jQuery, window);
