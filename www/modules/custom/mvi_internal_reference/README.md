# MVI Commerce Menu README

## Installation
- Enable this module, you will notice the following:
	- 1 new fields for the default Order:
		- Interne referentie :
			- Once filled in correctly, this can no longer be updated
			- added automatically to the form display & the default view display as replacement for the order id
			- if left empty when creating an order (when an anonymouse user places an order for example), this will be filled with 'AUTO-{order-id}'. If not (when a webmaster creates an order manually), this filled in value will be used, but must be unique.
			- All places in views where the order id is outputted it is replaced by the filled in Internal reference
			- All templates that run through the the theme hook "template_preprocess_commerce_order" will have there order_number replace by this internal reference
