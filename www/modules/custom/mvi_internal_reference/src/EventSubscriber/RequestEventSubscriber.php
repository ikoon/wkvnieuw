<?php

namespace Drupal\mvi_internal_reference\EventSubscriber;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Drupal\Core\Routing\CurrentRouteMatch;

use Drupal\mvi_internal_reference\OrderResolver;

/**
 * Request Event Subscriber.
 */
class RequestEventSubscriber implements EventSubscriberInterface {

  /**
   * The current route match object.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * The order resolver object
   *
   * @var \Drupal\mvi_internal_reference\OrderResolver
   */
  protected $orderResolver;


  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   * @param \Drupal\mvi_internal_reference\OrderResolver $order_resolver
   */
  public function __construct(CurrentRouteMatch $current_route_match, OrderResolver $order_resolver) {
    $this->currentRouteMatch = $current_route_match;
    $this->orderResolver = $order_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'kernel.request' => [
        ['redirectToCorrectOrder']
      ]
    ];
  }

  /**
   * Redirects any request to the detail of an order page, with the internal reference instead of the order number to the correct order detail (view or edit)
   * 
   * @param $event
   */
  public function redirectToCorrectOrder(Event $event) {
    $routeName = $this->currentRouteMatch->getRouteName();
    if ($routeName === 'system.404') {
      $uri = $event->getRequest()->getRequestUri();
      $pattern = '/^\/admin\/commerce\/orders\/([\w_\- ]+)\/?.*/';
      preg_match($pattern, $uri, $matches);
      // matches can be anything from "/AUTO-123" to "/AUTO-123/edit" to "/AUTO-123/clone", ...
      if ($matches && isset($matches[1])) {
        $internalRef = $matches[1];
        $orderId = $this->orderResolver->getOrderIdByInternalRef($internalRef);
        if (!is_null($orderId)) {
          $pattern = '/^(\/admin\/commerce\/orders\/)([\w_\- ]+)(\/?.*)/';
          $orderIdStr = strval($orderId);
          $newUri = preg_replace($pattern, '${1}' . $orderIdStr . '${3}', $uri);

          $response = new RedirectResponse($newUri);
          $response->send();
          return;
        }
      }
    }
  }
}