<?php

namespace Drupal\mvi_internal_reference;

use Drupal\commerce_order\Entity\Order;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * Class OrderResolver.
 */
class OrderResolver
{

  /**
   * The entity type manager
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The order storage
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $orderStorage;

  /**
   * Constructs a new OrderResolver object.
   * 
   */
  public function __construct(EntityTypeManager $entity_type_manager)
  {
    $this->entityTypeManager = $entity_type_manager;

    $this->orderStorage = $this->entityTypeManager->getStorage('commerce_order');
  }

  /**
   * @param string $internalRef
   * 
   * @return boolean True if the given internal reference is already used, false if not
   */
  public function isInternalReferenceTaken($internalRef) {
    $queryResult = $this->orderStorage->getQuery()
      ->condition('field_internal_reference', $internalRef)
      ->count()
      ->execute();
    
    return intval($queryResult) > 0;
  }

  /**
   * @param string $internalRef
   * 
   * @return int|null Order id if found, null if not
   */
  public function getOrderIdByInternalRef($internalRef) {
    $queryResult = $this->orderStorage->getQuery()
      ->condition('field_internal_reference', $internalRef)
      ->range(0, 1)
      ->execute();
    
    if (count($queryResult) >= 1) {
      $ids = array_keys($queryResult);
      return $ids[0];
    }
    return null;
  }

  /**
   * Fills in "AUTO-{Order ID}" for Internal reference, IF it is not filled in already
   * 
   * @param \Drupal\commerce_order\Entity\Order $order
   * 
   * @return void
   */
  public function setInternalReferenceIfNeeded(Order $order) {
    if ($order->hasField('field_internal_reference') && $order->get('field_internal_reference')->isEmpty()) {
      $newRef = 'AUTO-' . $order->id();

      $newOrder = $this->orderStorage->load($order->id());
      $newOrder->set('field_internal_reference', $newRef, false);
      $newOrder->save();

      \Drupal::logger('mvi_internal_reference')->notice('Automatically updated internal reference to "@ref".', ['@ref' => $newRef]);
    }
  }

  /**
   * Fills in "AUTO-{Order ID}" for Internal reference, for ALL orders in the DB, IF it is not filled in already
   * 
   * @param \Drupal\commerce_order\Entity\Order $order
   * 
   * @return void
   */
  public function setAllInternalReferencesIfNeeded() {
    $query = $this->orderStorage->getQuery()->condition('locked', 0);
    $ids = $query->execute();

    if (count($ids) > 0) {
      $orders = $this->orderStorage->loadMultiple(array_keys($ids));
      foreach ($orders as $order) {
        $this->setInternalReferenceIfNeeded($order);
      }
    }
  }
}
