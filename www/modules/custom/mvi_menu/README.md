# MVI Commerce Menu README

## Installation
- Enable this module, you will notice the following:
	- 1 new Product type:
		- Menu : To create a new menu, this product type should be used. You can add fields and play around with the form display of this Product type, DO NOT CREATE VARIATIONS MANUALLY, a single variation is created when needed automatically. The price will always be 0, this is so discounts / promotions / other price adjustments for menu child products are still respected!
	- 2 new Order item types:
		- Menu : When adding a product of type "Menu" to the card, this Order item type will be used.
		- Menu child: When adding a product of type "Menu" to the card, 1 Order item per course will be added automatically, which makes the total price correct. (Remember, the Menu order item's price will always be 0).
	- 1 new Content type:
		- Course Product : DO NOT CREATE ANY OF THESE MANUALLY, this is only used as ref when creating Products of type "Menu"
	- 1 new Taxonomy Vocabulary:
		- Menu course : This represents a single course, add as many terms (courses) here as you want
	- 3 new Views Field type (linked to entity type "Order item")
		- Edit Menu radios : To be able to update the selected courses in the shopping cart (or any other view)
			- go to the view (for shopping cart this is at /admin/structure/views/view/commerce_cart_form)
			- Add the new field "Edit Menu radios" and output it wherever you like
		- Menu unit price : To be able to show the calculated Menu order item price in the shopping cart (or any other view). Remember, the Menu order item's price in the database in always 0. So in order to show the correct price in the (default) shopping cart:
			- go to the view (for shopping cart this is at /admin/structure/views/view/commerce_cart_form)
			- Add the new field "Menu item price", configure to "Exclude from display"
			- Re-order, so the "Menu item price" field can be used in the rewrite results of "Global: Custom text" below
			- The result for the "Menu item price" field is NULL when the order item is not of type "Menu". This means in the rewrite results of the "Global: Custom text", you can conditionally render the correct price : {% if menu_price_views_field %}{{ menu_price_views_field }}{% else %}{{ unit_price__number }}{% endif %}
		- Menu details : To be able to show which product is selected for which course in the shopping cart (or any other view):
			- go to the view (for shopping cart this is at /admin/structure/views/view/commerce_cart_form)
			- Add the new field "Menu details" and output it wherever you like
	- 2 new Twig functions
		- get_order_item_unit_price(order_item) : Needs an order item as argument, returns NULL when the given order item is not a menu order item. Otherwise it returns a formatted price render array. Holding the calculated price for this menu order item
		- get_order_item_details(order_item) : Needs an order item as argument, returns NULL when the given order item is not a menu order item. Otherwise it returns an associative array with 1 key per course (the coursename). The value is the linked productname for that course. For example: ['Appetizer' => 'Shrimps', 'Main course' => 'Steak', 'Desert' => 'Chocomousse']

## Example
A Menu, named "Basic 3 courses menu" has 3 courses, "Appetizer", "Main course" and "Desert". The user should be able to choose 1 out of 3 appetizers, 1 out of 2 main courses and 1 out of 5 deserts.

- Create 3 new "Menu course" taxonomy terms, name them "Appetizer", "Main course" and "Desert".
- Create the 3 appetizers, 2 main courses and 5 deserts as default (or any other type, just not a menu) products, if they do not exist already
- Create 1 new Product of type "Menu" (/product/add/menu):
	- Title: "Basic 3 courses menu"
	- Courses: Create 1 per course, in this example 3:
		- 1: 
			- Course: "Appetizer"
			- Product: check the 3 possible appetizer products in the list of menu
		- 2: click "Add another item" to add the 2nd course
			- Course: "Main course"
			- Product: check the 2 possible main course products in the list of menu
		- 2: click "Add another item" to add the 3nd course
			- Course: "Desert"
			- Product: check the 5 possible deserts products in the list of menu
	- Body / tags / images: These fields are copied from the default product type, use them as you would at any other product.
- Go to the view page of this product, a new fom will appear where you must select 1 product per course to be able to add this product to your shopping cart. If you have allready overwritten the template, take a look at the template file "commerce-product--menu.html.twig" on how to output this form.
- Select one of each course and add to your shopping cart.
- Check if the cart has new form to update the courses (if not, edit the cart view, see last step at Installation above)
- Check in the CMS, at the "edit order" page, you will see 1 new Order item of type "Menu" (price 0) and 3 new Order Items of type "Menu Child"
- Check in the CMS, at the "view order" page, you will notice the title of the Menu item should now show "Basic 3 courses menu -- Appetizer: Shrimps, Main course: Steak, Desert: Chocomousse" for example
