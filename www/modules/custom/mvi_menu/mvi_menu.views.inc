<?php

/**
 * @file
 * Contains mvi_menu\mvi_menu.views.inc..
 * Provide a custom views field data that isn't tied to any other module. */


/**
* Implements hook_views_data().
*/
function mvi_menu_views_data() {
  $data['views']['table']['group'] = t('Custom Global');
  $data['views']['table']['join'] = [
    // #global is a special flag which allows a table to appear all the time.
    '#global' => [],
  ];

  $data['views']['menu_price_views_field'] = [
    'title' => t('Menu unit price'),
    'help' => t('Renders the calculated unit price for a menu order item, or NULL if the order item is not a menu'),
    'field' => [
      'id' => 'menu_price_views_field',
    ],
  ];

  $data['views']['menu_details_views_field'] = [
    'title' => t('Menu details'),
    'help' => t('Renders the menu child items for a menu order item, or NULL if the order item is not a menu'),
    'field' => [
      'id' => 'menu_details_views_field',
    ],
  ];
  return $data;
}
