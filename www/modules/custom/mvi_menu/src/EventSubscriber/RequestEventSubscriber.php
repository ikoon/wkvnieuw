<?php

namespace Drupal\mvi_menu\EventSubscriber;

// use Drupal\commerce_product\Entity\Product;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
// use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountProxy;

/**
 * Request Event Subscriber.
 */
class RequestEventSubscriber implements EventSubscriberInterface {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The current route match object.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * The current user object.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(MessengerInterface $messenger, CurrentRouteMatch $current_route_match, AccountProxy $current_user) {
    $this->messenger = $messenger;
    $this->currentRouteMatch = $current_route_match;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'kernel.request' => [
        ['denyVariationsAccess']
      ]
    ];
  }

  /**
   * @param $event
   * 
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   */
  public function denyVariationsAccess() {
    if ($this->currentRouteMatch->getRouteName() === 'entity.commerce_product_variation.collection') {
      /** @var \Drupal\commerce_product\Entity\Product */
      $product = $this->currentRouteMatch->getParameter('commerce_product');
      if ($product->bundle() === 'menu') {
        // admin just gets an error
        if ($this->currentUser->id() === '1') {
          $this->messenger->addError(t('Do NOT update the variations here manually, there should be one single variation with price 0'));
        } else {
          // others have got no access
          throw new AccessDeniedHttpException();
        }
      }
    }
  }
}