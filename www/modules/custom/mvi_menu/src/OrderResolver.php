<?php

namespace Drupal\mvi_menu;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\commerce_cart\CartManagerInterface;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_price\Price;

/**
 * Class OrderResolver.
 */
class OrderResolver
{
  use StringTranslationTrait;

  /**
   * The cart manager.
   *
   * @var \Drupal\commerce_cart\CartManagerInterface
   */
  protected $cartManager;

  /**
   * The entity type manager
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The cart provider.
   *
   * @var \Drupal\commerce_cart\CartProviderInterface
   */
  protected $cartProvider;

  /**
   * The node storage
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * The taxonomy_term storage
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $taxonomyTermStorage;

  /**
   * Constructs a new OrderResolver object.
   * 
   * @param \Drupal\commerce_cart\CartManagerInterface $cart_manager
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   */
  public function __construct(CartManagerInterface $cart_manager, EntityTypeManager $entity_type_manager, CartProviderInterface $cart_provider)
  {
    $this->cartManager = $cart_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->cartProvider = $cart_provider;

    $this->nodeStorage = $this->entityTypeManager->getStorage('node');
    $this->taxonomyTermStorage = $this->entityTypeManager->getStorage('taxonomy_term');
  }

  /**
   * Gets the children 'menu_child' order items from a parent 'menu' order item
   * 
   * @param \Drupal\commerce_order\Entity\Order $order
   * @param \Drupal\commerce_order\Entity\OrderItem $parentOrderItem
   * 
   * @return array array of \Drupal\commerce_order\Entity\OrderItem
   */
  public function getMenuChildOrderItemsFromParent(Order $order, OrderItem $parentOrderItem) {
    if ($parentOrderItem->bundle() !== 'menu') return [];

    $result = [];
    
    $allOrderItems = $order->getItems();
    foreach ($allOrderItems as $orderItem) {
      if ($orderItem->bundle() === 'menu_child') {
        $parent = $orderItem->get('field_parent')->first()->getValue();
        $parentId = $parent['target_id'];
        if ($parentId === $parentOrderItem->id()) {
          $result[] = $orderItem;
        }
      }
    }

    return $result;
  }

  /**
   * Gets the parent 'menu' order item from the given 'menu_child' order item
   * 
   * @param \Drupal\commerce_order\Entity\Order $order
   * @param \Drupal\commerce_order\Entity\OrderItem $childOrderItem
   * 
   * @return \Drupal\commerce_order\Entity\OrderItem|null the parent if found, null if not found
   */
  private function getMenuParentFromChild(Order $order, OrderItem $childOrderItem) {
    if ($childOrderItem->bundle() !== 'menu_child') return null;

    $parent = $childOrderItem->get('field_parent')->first()->getValue();
    $parentId = $parent['target_id'];

    $allOrderItems = $order->getItems();
    foreach ($allOrderItems as $orderItem) {
      if ($orderItem->id() === $parentId) {
        return $orderItem;
      }
    }
    return null;
  }

  /**
   * Edits quantity of OR removes menu_child order items for the given order
   * 
   * @param \Drupal\commerce_order\Entity\Order $cart
   */
  public function restoreMenuChildOrderItems(Order $cart) {
    $allOrderItems = $cart->getItems();
    foreach ($allOrderItems as $orderItem) {
      if ($orderItem->bundle() === 'menu_child') {
        // remove menu children if no parent was found
        $parentOrderItem = $this->getMenuParentFromChild($cart, $orderItem);
        if (is_null($parentOrderItem)) {
          // should remove this order item
          $this->cartManager->removeOrderItem($cart, $orderItem, false);
        } else {
          // should update the quantity, if is not in sync:
          $strCorrectQuantity = $parentOrderItem->getQuantity();
          $correctQuantity = floatval($strCorrectQuantity);
          $newQuantity = floatval($orderItem->getQuantity());
          if ($newQuantity !== $correctQuantity) {
            $orderItem->setQuantity($strCorrectQuantity);
            $this->cartManager->updateOrderItem($cart, $orderItem, false);
          }
        }
      }
    }
  }

  /**
   * @param \Drupal\commerce_order\Entity\Order $order
   * @param \Drupal\commerce_order\Entity\OrderItem $menuOrderItem
   */
  public function getMenuPrice(Order $order, OrderItem $menuOrderItem) {
    if ($menuOrderItem->bundle() !== 'menu') {
      throw new \Exception('Trying to calculate order item price for order item of type "' . $menuOrderItem->bundle() . '", only type "menu" is allowed!');
    }

    $children = $this->getMenuChildOrderItemsFromParent($order, $menuOrderItem);
    $totalPrice = new Price(0, $order->getStore()->getDefaultCurrencyCode());
    foreach($children as $child) {
      $totalPrice = $totalPrice->add($child->getPurchasedEntity()->getPrice());
    }
    return $totalPrice;
  }


  /**
   * Gets the correct count filtering menu_child order items out
   * 
   * @return array With 2 keys, 'count' (integer) & 'count_text' (string)
   */
  public function getDataForCommerceCartBlock() {
    /** @var \Drupal\commerce_order\Entity\OrderInterface[] $carts */
    $carts = $this->cartProvider->getCarts();
    $carts = array_filter($carts, function ($cart) {
      /** @var \Drupal\commerce_order\Entity\OrderInterface $cart */
      // There is a chance the cart may have converted from a draft order, but
      // is still in session. Such as just completing check out. So we verify
      // that the cart is still a cart.
      return $cart->hasItems() && $cart->cart->value;
    });

    $count = 0;
    if (!empty($carts)) {
      foreach ($carts as $cart_id => $cart) {
        foreach ($cart->getItems() as $order_item) {
          if ($order_item->bundle() !== 'menu_child') {
            $count += (int) $order_item->getQuantity();
          }
        }
      }
    }

    return [
      'count' => $count,
      'count_text' => $this->formatPlural($count, '@count item', '@count items'),
    ];
  }

  /**
   * Given an order and a menu order item, returns the courses and titles of menu_child order items, for example:
   * "appetizer: Shrimp, main course: Steak, desert: Tiramisu"
   * 
   * @param \Drupal\commerce_order\Entity\Order $order
   * @param \Drupal\commerce_order\Entity\OrderItem $menuOrderItem
   * 
   * @return array per course 1 key, the value is the menu_child's label for this course
   */
  public function getMenuDetail(Order $order, OrderItem $menuOrderItem) {
    $children = $this->getMenuChildOrderItemsFromParent($order, $menuOrderItem);
    $details = [];
    foreach($children as $child) {
      $courseVal = $child->get('field_course')->first()->getValue();
      $course = $this->taxonomyTermStorage->load($courseVal['target_id']);
      $details[$course->label()] = $child->getPurchasedEntity()->label();
    }
    return $details;
  }
}
