<?php

namespace Drupal\mvi_menu\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\node\Entity\Node;
// use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Session\AccountProxy;

use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_price\Price;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_cart\CartManagerInterface;
use Drupal\commerce_cart\CartProvider;
// use Drupal\commerce_order\Adjustment;
use Drupal\commerce_store\CurrentStore;

/**
 * Class AddMenuToCartForm.
 */
class AddMenuToCartForm extends FormBase {


  /**
   * The entity type manager
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The cart manager.
   *
   * @var \Drupal\commerce_cart\CartManagerInterface
   */
  protected $cartManager;

  /**
   * The cart provider.
   *
   * @var \Drupal\commerce_cart\CartProvider
   */
  protected $cartProvider;
  /**
   * The store provider.
   *
   * @var \Drupal\commerce_store\CurrentStore
   */
  protected $storeProvider;
  /**
   * The current user
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * The node storage
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * The taxonomy_term storage
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $taxonomyTermStorage;

  /**
   * The taxonomy_term storage
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $productStorage;
  /**
   * The taxonomy_term storage
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $productVariationStorage;

  /**
   * Class constructor.
   */
  public function __construct(EntityTypeManager $entity_type_manager, CartManagerInterface $cart_manager, CartProvider $cart_provider, CurrentStore $store_provider, AccountProxy $current_user) {
    $this->entityTypeManager = $entity_type_manager;
    $this->cartManager = $cart_manager;
    $this->cartProvider = $cart_provider;
    $this->storeProvider = $store_provider;
    $this->currentUser = $current_user;

    $this->nodeStorage = $this->entityTypeManager->getStorage('node');
    $this->taxonomyTermStorage = $this->entityTypeManager->getStorage('taxonomy_term');
    $this->productVariationStorage = $this->entityTypeManager->getStorage('commerce_product_variation');
    $this->productStorage = $this->entityTypeManager->getStorage('commerce_product');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('commerce_cart.cart_manager'),
      $container->get('commerce_cart.cart_provider'),
      $container->get('commerce_store.current_store'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'add_menu_to_cart_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Product $product = null) {
    if (is_null($product)) {
      throw new \Exception(t('The form mvi_menu.add_menu_to_cart_form requires a product as argument.'));
    }

    if ($product->bundle() !== 'menu') {
      throw new \Exception(t('The form mvi_menu.add_menu_to_cart_form requires a product of type "menu" as argument.'));
    }

    $courseProductsIds = array_map(function ($i) {
      return $i['target_id'];
    }, $product->get('field_course_product_variations')->getValue());

    $courseProducts = $this->nodeStorage->loadMultiple($courseProductsIds);

    foreach ($courseProducts as $i => $courseProduct) {
      $course = $courseProduct->get('field_course')->first()->getValue();
      $courseTerm = $this->taxonomyTermStorage->load($course['target_id']);
      $fieldsetKey = 'course_product_' . $course['target_id'];

      $productVariationIds = array_map(function ($i) {
        return $i['target_id'];
      }, $courseProduct->get('field_product_variations')->getValue());

      $productVariations = $this->productVariationStorage->loadMultiple($productVariationIds);

      $options = [];
      foreach($productVariations as $productVariation) {
        $options[$productVariation->id()] = $productVariation->label() . ' - ' . $productVariation->getPrice();
      }

      $form[$fieldsetKey] = [
        '#type' => 'radios',
        '#title' => $courseTerm->label(),
        '#options' => $options,
        '#required' => true
      ];
    }

    $form['quantity'] = [
      '#type' => 'number',
      '#title' => t('Quantity'),
      '#default_value' => 1,
      '#min' => 1,
    ];

    $form['product_id'] = [
      '#type' => 'hidden',
      '#value' => $product->id(),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add to cart'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {
      // @TODO: Validate fields.
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $menuProduct = $this->productStorage->load($values['product_id']);
    $cart = $this->getCurrentCart();

    // - create one single product variation for the product of type "menu", with price "0" if needed:
    $menuProductVariation = $menuProduct->getDefaultVariation();
    if (!$menuProductVariation) {
      $menuProductVariation = ProductVariation::create([
        'type' => 'menu',
        'price' => new Price(0, $this->storeProvider->getStore()->getDefaultCurrencyCode()),
        'title' => $menuProduct->label(),
        'sku' => Html::cleanCssIdentifier($menuProduct->label()),
      ]);
      $menuProduct->addVariation($menuProductVariation);
      $menuProduct->save();
    }

    // - select this as purchasable entity to add (just like a normal product)
    // - create an order item for this (type "menu") and add it to the cart, then keep the id of this order item as reference
    $orderItemMenu = OrderItem::create([
      'type' => 'menu',
      'quantity' => intval($values['quantity']),
      'purchased_entity' => $menuProductVariation,
      'title' => $menuProduct->label(),
    ]);

    // add this order item to the cart (and save)!
    $orderItemMenu = $this->cartManager->addOrderItem($cart, $orderItemMenu, false);
    
    // use form data to create 1 OrderItem of type "menu_child" per course
    foreach ($values as $key => $value) {
      if (substr($key, 0, strlen('course_product_')) === 'course_product_') {
        $courseId = substr($key, strlen('course_product_'));
        $course = $this->taxonomyTermStorage->load($courseId);
        $productVariation = $this->productVariationStorage->load($value);
        $orderItemChild = OrderItem::create([
          'type' => 'menu_child',
          'quantity' => intval($values['quantity']),
          'purchased_entity' => $productVariation,
          'title' => $course->label() . ' - ' . $productVariation->label(),
          'field_course' => [$course],
          'field_parent' => [$orderItemMenu]
        ]);
        // add this order item to the cart (and save)!
        $this->cartManager->addOrderItem($cart, $orderItemChild, false);
      }
    }
  }

  private function getCurrentCart() {
    $account = $this->currentUser->getAccount();
    $store = $this->storeProvider->getStore();
    $cart = $this->cartProvider->getCart('default', $store, $account);
    if (!$cart) {
      $cart = $this->cartProvider->createCart('default', $store, $account);
    }
    return $cart;
  }
}
