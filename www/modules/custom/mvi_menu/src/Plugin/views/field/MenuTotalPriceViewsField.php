<?php

namespace Drupal\mvi_menu\Plugin\views\field;

use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\mvi_menu\OrderResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("menu_total_price_views_field")
 */
class MenuTotalPriceViewsField extends FieldPluginBase {

  /**
   * @var \Drupal\mvi_menu\OrderResolver
   */
  protected $orderResolver;


  /**
   * Constructs a new MenuPriceViewsField object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_cart\CartManagerInterface $cart_manager
   * @param \Drupal\mvi_menu\OrderResolver $order_resolver
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, OrderResolver $order_resolver) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->orderResolver = $order_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('mvi_menu.order_resolver')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    if (isset($values->_relationship_entities['order_items'])) {
      $orderItem = $values->_relationship_entities['order_items'];
      if ($orderItem->bundle() === 'menu') {
        $order = $orderItem->getOrder();
        $price = $this->orderResolver->getMenuPrice($order, $orderItem);
        $qty = $orderItem->getQuantity();
        $price = $price->multiply($qty);
        return [
          '#type' => 'inline_template',
          '#template' => '{{ price|commerce_price_format }}',
          '#context' => [
            'price' => $price,
          ],
        ];
      }
    }
    return null;
  }

}
