<?php

namespace Drupal\mvi_menu\Plugin\views\field;

use Drupal\commerce_cart\CartManagerInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\UncacheableFieldHandlerTrait;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\mvi_menu\OrderResolver;

/**
 * Defines a form element for updating the product per course for a menu item
 *
 * @ViewsField("commerce_order_item_edit_menu")
 */
class EditMenu extends FieldPluginBase {

  use UncacheableFieldHandlerTrait;

  /**
   * The cart manager.
   *
   * @var \Drupal\commerce_cart\CartManagerInterface
   */
  protected $cartManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * @var \Drupal\mvi_menu\OrderResolver
   */
  protected $orderResolver;

  /**
   * Constructs a new EditMenu object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_cart\CartManagerInterface $cart_manager
   * @param \Drupal\mvi_menu\OrderResolver $order_resolver
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CartManagerInterface $cart_manager, EntityTypeManagerInterface $entity_type_manager, OrderResolver $order_resolver, MessengerInterface $messenger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->cartManager = $cart_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->orderResolver = $order_resolver;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('commerce_cart.cart_manager'),
      $container->get('entity_type.manager'),
      $container->get('mvi_menu.order_resolver'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function clickSortable() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ResultRow $row, $field = NULL) {
    return '<!--form-item-' . $this->options['id'] . '--' . $row->index . '-->';
  }

  /**
   * Returns a token placeholder for the current field.
   *
   * @return string
   *   A token placeholder.
   */
  protected function getFieldTokenPlaceholder() {
    return parent::getFieldTokenPlaceholder();
    // return '{{ ' . $this->options['id'] . ' }}';
  }

  /**
   * Form constructor for the views form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function viewsForm(array &$form, FormStateInterface $form_state) {
    // Make sure we do not accidentally cache this form.
    $form['#cache']['max-age'] = 0;
    // The view is empty, abort.
    if (empty($this->view->result)) {
      unset($form['actions']);
      return;
    }

    $nodeStorage = $this->entityTypeManager->getStorage('node');
    $taxonomyTermStorage = $this->entityTypeManager->getStorage('taxonomy_term');
    $productVariationStorage = $this->entityTypeManager->getStorage('commerce_product_variation');

    $form[$this->options['id']]['#tree'] = TRUE;
    foreach ($this->view->result as $row_index => $row) {

      $form[$this->options['id']][$row_index] = [
        '#type' => 'container',
        '#name' => 'edit-menu-' . $row_index,
        '#update_menus' => TRUE,
        '#row_index' => $row_index,
        '#attributes' => ['class' => ['edit-menu']],
      ];

      /** @var \Drupal\commerce_order\Entity\OrderItemInterface $orderItem */
      $orderItem = $this->getEntity($row);
      if ($orderItem->bundle() === 'menu') {
        // @TODO, quite some duplicate code in "AddMenuToCartForm.php" ...
        $product = $orderItem->getPurchasedEntity()->getProduct();
        $courseProductsIds = array_map(function ($i) {
          return $i['target_id'];
        }, $product->get('field_course_product_variations')->getValue());

        $courseProducts = $nodeStorage->loadMultiple($courseProductsIds);

        $defaultValuesByCourseId = $this->getCurrentValues($orderItem);

        foreach ($courseProducts as $i => $courseProduct) {
          $course = $courseProduct->get('field_course')->first()->getValue();
          $courseTerm = $taxonomyTermStorage->load($course['target_id']);
          $fieldsetKey = 'course_product_' . $course['target_id'];

          $productVariationIds = array_map(function ($i) {
            return $i['target_id'];
          }, $courseProduct->get('field_product_variations')->getValue());

          $productVariations = $productVariationStorage->loadMultiple($productVariationIds);

          $options = [];
          foreach($productVariations as $productVariation) {
            $options[$productVariation->id()] = $productVariation->label() . ' - ' . $productVariation->getPrice();
          }

          $form[$this->options['id']][$row_index][$fieldsetKey] = [
            '#type' => 'radios',
            '#title' => $courseTerm->label(),
            '#options' => $options,
            '#required' => true,
            '#default_value' => $defaultValuesByCourseId[$course['target_id']],
          ];
        }
      }
    }
  }

  private function getCart() {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $cart */
    $orderStorage = $this->entityTypeManager->getStorage('commerce_order');
    return $orderStorage->load($this->view->argument['order_id']->getValue());
  }

  private function getCurrentValues(OrderItem $orderItem) {
    $cart = $this->getCart();
    $menuChildOrders = $this->orderResolver->getMenuChildOrderItemsFromParent($cart, $orderItem);
    $defaultValuesByCourseId = [];
    foreach ($menuChildOrders as $childMenuOrderItem) {
      $courseTid = $childMenuOrderItem->get('field_course')->first()->getValue()['target_id'];
      $defaultValuesByCourseId[$courseTid] = $childMenuOrderItem->getPurchasedEntity()->id();
    }
    return $defaultValuesByCourseId;
  }

  /**
   * Submit handler for the views form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function viewsFormSubmit(array &$form, FormStateInterface $form_state) {
    $taxonomyTermStorage = $this->entityTypeManager->getStorage('taxonomy_term');
    $productVariationStorage = $this->entityTypeManager->getStorage('commerce_product_variation');
    $values = $form_state->getValue('edit_menu');
    foreach ($values as $rowIndex => $arr) {
      /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
      $orderItem = $this->getEntity($this->view->result[$rowIndex]);

      // build new valuesByCourseId
      $newValuesByCourseId = [];
      foreach($arr as $key => $value) {
        $newValuesByCourseId[substr($key, strlen('course_product_'))] = $value;
      }

      $cartUpdated = false;
      $cart = $this->getCart();
      $menuChildOrderItems = $this->orderResolver->getMenuChildOrderItemsFromParent($cart, $orderItem);
      foreach ($menuChildOrderItems as $childMenuOrderItem) {
        $courseTid = $childMenuOrderItem->get('field_course')->first()->getValue()['target_id'];
        $course = $taxonomyTermStorage->load($courseTid);
        $productVariation = $childMenuOrderItem->getPurchasedEntity();
        if ($newValuesByCourseId[$courseTid] !== $productVariation->id()) {
          $newProductVariationId = $newValuesByCourseId[$courseTid];
          $newProductVariation = $productVariationStorage->load($newProductVariationId);
          
          $childMenuOrderItem->setTitle($course->label() . ' - ' . $newProductVariation->label());
          $childMenuOrderItem->set('purchased_entity', $newProductVariation);
          $this->cartManager->updateOrderItem($cart, $childMenuOrderItem);
          $cartUpdated = true;
        }
      }
        
      if ($cartUpdated) {
        $this->messenger->addMessage($this->t('Your shopping cart has been updated.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing.
  }

}
