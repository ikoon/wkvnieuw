<?php

namespace Drupal\mvi_menu\TwigExtension;

use Drupal\commerce_order\Entity\OrderItem;
use Drupal\mvi_menu\OrderResolver;

/**
 * Class MviMenuTwigExtensions.
 */
class MviMenuTwigExtensions extends \Twig_Extension {

  /**
   * @var \Drupal\mvi_menu\OrderResolver
   */
  protected $orderResolver;

  /**
   * Constructs twig extension.
   *
   * @param \Drupal\mvi_menu\OrderResolver $orderResolver
   */
  public function __construct(OrderResolver $orderResolver) {
    $this->orderResolver = $orderResolver;
  }

  public function get_order_item_unit_price (OrderItem $orderItem) {
    if ($orderItem->bundle() === 'menu') {
      $order = $orderItem->getOrder();
      $price = $this->orderResolver->getMenuPrice($order, $orderItem);
      return [
        '#type' => 'inline_template',
        '#template' => '{{ price|commerce_price_format }}',
        '#context' => [
          'price' => $price,
        ],
      ];
    }
    return null;
  }

  public function get_order_item_details (OrderItem $orderItem) {
    if ($orderItem->bundle() === 'menu') {
      $order = $orderItem->getOrder();
      return $this->orderResolver->getMenuDetail($order, $orderItem);
    }
    return null;
  }

   /**
    * {@inheritdoc}
    */
    public function getTokenParsers() {
      return [];
    }

   /**
    * {@inheritdoc}
    */
    public function getNodeVisitors() {
      return [];
    }

   /**
    * {@inheritdoc}
    */
    public function getFilters() {
      return [];
    }

   /**
    * {@inheritdoc}
    */
    public function getTests() {
      return [];
    }

   /**
    * {@inheritdoc}
    */
    public function getFunctions() {
      return [
        new \Twig_SimpleFunction('get_order_item_unit_price', array($this, 'get_order_item_unit_price')),
        new \Twig_SimpleFunction('get_order_item_details', array($this, 'get_order_item_details')),
      ];
    }

   /**
    * {@inheritdoc}
    */
    public function getOperators() {
      return [];
    }

   /**
    * {@inheritdoc}
    */
    public function getName() {
      return 'mvi_menu.twig_extensions';
    }

}
