(function ($, Drupal) {

    Drupal.behaviors.shippingInformation = {
        attach: function (context, settings) {

            $('fieldset[id^="edit-mvi-shipping-information-shipments-"]').find('input').each(function(){
                if($(this).is(':checked')){
                    var shippingMethodVal = $(this).val();
                    if(shippingMethodVal != '2--default'){
                        $('#shipping-information-wrapper').addClass('show-shipping-profile');
                    } else {
                        // Pickup at store selected.
                        $('#shipping-information-wrapper').removeClass('show-shipping-profile');
                        // Default fill in the default fields.
                        var storeAddress = settings.storeAddress;
                        $('div[id^="edit-mvi-shipping-information-shipping-profile-address-0-address"]').find('input[id^="edit-mvi-shipping-information-shipping-profile-address-0-address-given-name"]').val("Wouters");
                        $('div[id^="edit-mvi-shipping-information-shipping-profile-address-0-address"]').find('input[id^="edit-mvi-shipping-information-shipping-profile-address-0-address-family-name"]').val("Kwaliteitsvis");
                        $('div[id^="edit-mvi-shipping-information-shipping-profile-address-0-address"]').find('input[id^="edit-mvi-shipping-information-shipping-profile-address-0-address-address-line1"]').val(storeAddress.address_line1);
                        $('div[id^="edit-mvi-shipping-information-shipping-profile-address-0-address"]').find('input[id^="edit-mvi-shipping-information-shipping-profile-address-0-address-postal-code"]').val(storeAddress.postal_code);
                        $('div[id^="edit-mvi-shipping-information-shipping-profile-address-0-address"]').find('input[id^="edit-mvi-shipping-information-shipping-profile-address-0-address-locality"]').val(storeAddress.locality);
                        $('div[id^="edit-mvi-shipping-information-shipping-profile"]').find('input[id^="edit-mvi-shipping-information-shipping-profile-field-phone-0-value"]').val('000 000 000');
                    }
                }

                $(this).click(function(){
                    var shippingMethodVal = $(this).val();
                    if(shippingMethodVal != '2--default'){
                        $('#shipping-information-wrapper').addClass('show-shipping-profile');
                        $(context, 'div[id^="edit-mvi-shipping-information-shipping-profile-address-0-address"]').find('input[id^="edit-mvi-shipping-information-shipping-profile-address-0-address-given-name"]').val("");
                        $(context, 'div[id^="edit-mvi-shipping-information-shipping-profile-address-0-address"]').find('input[id^="edit-mvi-shipping-information-shipping-profile-address-0-address-family-name"]').val("");
                        $(context, 'div[id^="edit-mvi-shipping-information-shipping-profile-address-0-address"]').find('input[id^="edit-mvi-shipping-information-shipping-profile-address-0-address-address-line1"]').val("");
                        $(context, 'div[id^="edit-mvi-shipping-information-shipping-profile-address-0-address"]').find('input[id^="edit-mvi-shipping-information-shipping-profile-address-0-address-postal-code"]').val("");
                        $(context, 'div[id^="edit-mvi-shipping-information-shipping-profile-address-0-address"]').find('input[id^="edit-mvi-shipping-information-shipping-profile-address-0-address-locality"]').val("");
                        $(context, 'div[id^="edit-mvi-shipping-information-shipping-profile"]').find('input[id^="edit-mvi-shipping-information-shipping-profile-field-phone-0-value"]').val("");
                        if($('input[id^="edit-mvi-shipping-information-shipping-profile-edit-button"]').length){
                            $('input[id^="edit-mvi-shipping-information-shipping-profile-edit-button"]').mousedown();
                        }
                    } else {
                        // Pickup shipping method selected.
                        $('#shipping-information-wrapper').removeClass('show-shipping-profile');
                        if($('div[id^="edit-mvi-shipping-information-shipping-profile-address-0-address"]').length){
                            var storeAddress = settings.storeAddress;
                            $('div[id^="edit-mvi-shipping-information-shipping-profile-address-0-address"]').find('input[id^="edit-mvi-shipping-information-shipping-profile-address-0-address-given-name"]').val("Wouters");
                            $('div[id^="edit-mvi-shipping-information-shipping-profile-address-0-address"]').find('input[id^="edit-mvi-shipping-information-shipping-profile-address-0-address-family-name"]').val("Kwaliteitsvis");
                            $('div[id^="edit-mvi-shipping-information-shipping-profile-address-0-address"]').find('input[id^="edit-mvi-shipping-information-shipping-profile-address-0-address-address-line1"]').val(storeAddress.address_line1);
                            $('div[id^="edit-mvi-shipping-information-shipping-profile-address-0-address"]').find('input[id^="edit-mvi-shipping-information-shipping-profile-address-0-address-postal-code"]').val(storeAddress.postal_code);
                            $('div[id^="edit-mvi-shipping-information-shipping-profile-address-0-address"]').find('input[id^="edit-mvi-shipping-information-shipping-profile-address-0-address-locality"]').val(storeAddress.locality);
                            $('div[id^="edit-mvi-shipping-information-shipping-profile"]').find('input[id^="edit-mvi-shipping-information-shipping-profile-field-phone-0-value"]').val('000 000 000');
                        }
                    }
                });
            });
        }
    };

})(jQuery, Drupal);
