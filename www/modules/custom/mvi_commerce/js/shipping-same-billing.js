(function ($, Drupal) {

    Drupal.behaviors.reuseBillingBehavior= {
        attach: function (context, settings) {
            $selectIds = {
                'edit-payment-information-billing-information-address-0-address-country-code': 'edit-shipping-information-shipping-profile-address-0-address-country-code',
            };
            $inputIds = {
                'edit-payment-information-billing-information-address-0-address-organization': 'edit-shipping-information-shipping-profile-address-0-address-organization',
                'edit-payment-information-billing-information-address-0-address-given-name': 'edit-shipping-information-shipping-profile-address-0-address-given-name',
                'edit-payment-information-billing-information-address-0-address-family-name': 'edit-shipping-information-shipping-profile-address-0-address-family-name',
                'edit-payment-information-billing-information-address-0-address-address-line1': 'edit-shipping-information-shipping-profile-address-0-address-address-line1',
                'edit-payment-information-billing-information-address-0-address-address-line2': 'edit-shipping-information-shipping-profile-address-0-address-address-line2',
                'edit-payment-information-billing-information-address-0-address-postal-code': 'edit-shipping-information-shipping-profile-address-0-address-postal-code',
                'edit-payment-information-billing-information-address-0-address-locality': 'edit-shipping-information-shipping-profile-address-0-address-locality',
                'edit-payment-information-billing-information-field-phone-0-value': 'edit-shipping-information-shipping-profile-field-phone-0-value',
            };

            $.each($selectIds, function(key, target){
                $('[id^="' + key + '"]').once().change(function(){
                    if($('[id^="edit-shipping-information-reuse-billing"]').is(':checked')){
                        $val = $(this).val();
                        $('[id^="' + target + '"]').val($val);
                    }
                });
            });

            $.each($inputIds, function(key, target){
                $('[id^="' + key + '"]').once().keyup(function(){
                    if($('[id^="edit-shipping-information-reuse-billing"]').is(':checked')){
                        $val = $(this).val();
                        $('[id^="' + target + '"]').val($val);
                    }
                });
            });

            $('.form-item-shipping-information-reuse-billing', context).click(function () {

                $.each($inputIds, function(key, target){
                    $val = $('[id^="' + key + '"]').val();
                    $('[id^="' + target + '"]').val($val);
                });
                $.each($selectIds, function(key, target){
                    $val = $('[id^="' + key + '"]').find(':selected').val();
                    $('[id^="' + target + '"]').val($val);
                });
            });



        }
    };

})(jQuery, Drupal);
