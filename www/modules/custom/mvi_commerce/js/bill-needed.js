(function ($, Drupal) {

    Drupal.behaviors.billNeededBehavior= {
        attach: function (context, settings) {
            if($('[id^="edit-payment-information-billing-information-bill-needed"]').length){
                var toggleBillNeededElement = $('[id^="edit-payment-information-billing-information-bill-needed"]');
                var billNeeded = toggleBillNeededElement.is(":checked");
                if(billNeeded){
                    $('.field--name-field-company').css('display', 'block');
                    $('.field--name-field-vat').css('display', 'block');
                }

                toggleBillNeededElement.on('change', function(){
                    var billNeeded = toggleBillNeededElement.is(":checked");
                    if(billNeeded){
                        $('.field--name-field-company').css('display', 'block');
                        $('.field--name-field-vat').css('display', 'block');
                    } else {
                        $('.field--name-field-company').css('display', 'none');
                        $('.field--name-field-vat').css('display', 'none');

                        $('.field--name-field-company input').val('');
                        $('.field--name-field-vat input').val('');
                    }
                });
            }
        }
    };

})(jQuery, Drupal);