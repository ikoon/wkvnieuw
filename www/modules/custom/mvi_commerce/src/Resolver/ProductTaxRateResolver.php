<?php

namespace Drupal\mvi_commerce\Resolver;

use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_tax\Resolver\TaxRateResolverInterface;
use Drupal\commerce_tax\TaxZone;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Returns the tax rate selected on the purchased entity.
 */
class ProductTaxRateResolver implements TaxRateResolverInterface {

  /**
   * {@inheritdoc}
   *
   * Resolve the Tax type for the product based on the vat code coming from Exact online.
   *
   * 5 = standard
   * 3 = reduced
   */
  public function resolve(TaxZone $zone, OrderItemInterface $order_item, ProfileInterface $customer_profile) {
    $rates = $zone->getRates();

    // Get the purchased entity.
    /** @var ProductVariationInterface $item*/
    $item = $order_item->getPurchasedEntity();

    if($item instanceof ProductVariationInterface && $item->bundle() != 'menu') {
      $rate_id = $item->field_tax_rate->value;

      if($rate_id != 'none') {
        foreach ($rates as $rate) {
          if ($rate->getId() == $rate_id) {
            return $rate;
          }
        }
      }
    }

    // If no rate has been found, let's others resolvers try to get it.
    return NULL;
  }

}
