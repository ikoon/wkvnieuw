<?php

namespace Drupal\mvi_commerce\Mail;

use Drupal\commerce\MailHandlerInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Mail\OrderReceiptMailInterface;
use Drupal\commerce_order\OrderTotalSummaryInterface;
use Drupal\commerce_shipping\Entity\Shipment;
use Drupal\commerce_shipping\Entity\ShippingMethodInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class OrderReceiptMail implements OrderReceiptMailInterface {

  use StringTranslationTrait;

  /**
   * The order type entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $orderTypeStorage;

  /**
   * The mail handler.
   *
   * @var \Drupal\commerce\MailHandlerInterface
   */
  protected $mailHandler;

  /**
   * The order total summary.
   *
   * @var \Drupal\commerce_order\OrderTotalSummaryInterface
   */
  protected $orderTotalSummary;

  /**
   * The profile view builder.
   *
   * @var \Drupal\profile\ProfileViewBuilder
   */
  protected $profileViewBuilder;

  /**
   * Constructs a new OrderReceiptMail object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce\MailHandlerInterface $mail_handler
   *   The mail handler.
   * @param \Drupal\commerce_order\OrderTotalSummaryInterface $order_total_summary
   *   The order total summary.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   The mail manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MailHandlerInterface $mail_handler, OrderTotalSummaryInterface $order_total_summary) {
    $this->orderTypeStorage = $entity_type_manager->getStorage('commerce_order_type');
    $this->mailHandler = $mail_handler;
    $this->orderTotalSummary = $order_total_summary;
    $this->profileViewBuilder = $entity_type_manager->getViewBuilder('profile');
  }

  /**
   * {@inheritdoc}
   */
  public function send(OrderInterface $order, $to = NULL, $bcc = NULL) {
    /** @var \Drupal\commerce_order\Entity\OrderTypeInterface $order_type */
    $order_type = $this->orderTypeStorage->load($order->bundle());
    if (!$order_type->shouldSendReceipt()) {
      return FALSE;
    }
    $to = $order->getEmail();
    if (!$to) {
      // The email should not be empty, unless the order is malformed.
      return FALSE;
    }

    $params = [
      'headers' => [
        'Content-Type' => 'text/html; charset=UTF-8;',
        'Content-Transfer-Encoding' => '8Bit'
      ],
      'from' => $order->getStore()->getName() . '<' . $order->getStore()->getEmail() . '>',
      'order' => $order,
    ];
    $subject = $this->t('Thankyou for your order: @number', ['@number' => $order->getOrderNumber()]);

    if ($receipt_bcc = $order_type->getReceiptBcc()) {
      $params['headers']['Bcc'] = $receipt_bcc;
    }

    $build = [
      '#theme' => 'commerce_order_receipt',
      '#order_entity' => $order,
      '#totals' => $this->orderTotalSummary->buildTotals($order)
    ];

    if($shipments = $order->get('shipments')->getValue()) {
      $shipmentTarget = $shipments[0];
      $shipment = Shipment::load($shipmentTarget['target_id']);
      /** @var ShippingMethodInterface $shippingMethod */
      $shippingMethod = $shipment->getShippingMethod();

      $id = $shippingMethod->id();
      if($id == '2'){
        $shippingMethodType = 'pickup';
      } else {
        $shippingMethodType = 'delivery';
      }
      $build['#shipping_method'] = [
        'label' => $shippingMethod->label(),
        'type' => $shippingMethodType,
      ];
    }

    // Provide the pickup information if the mvi_pickup module is enabled
    if($shippingMethodType == 'pickup' && $order->hasField('field_pickup_from') && $order->hasField('field_pickup_to')) {
      $date = $order->get('field_pickup_from')->getValue();
      $from = $order->get('field_pickup_from')->getValue();
      $till = $order->get('field_pickup_to')->getValue();
      $pickupInformation = t("Pickup on: @pickup_date between @start and @end.", [
          '@pickup_date' =>  date('d/m/Y', $date[0]['value']),
          '@start' => date('H:i', $from[0]['value']),
          '@end' => date('H:i',$till[0]['value'])
        ]);
      $build['#pickup_information'] = $pickupInformation;
    }

    if($shippingMethodType == 'delivery' && $order->hasField('field_preferred_delivery')){
      $deliveryDate = $order->get('field_preferred_delivery')->getValue();
      $build['#delivery_information'] = t("Delivery on @delivery_date.", [
        '@delivery_date' => date('d/m/Y', $deliveryDate[0]['value'])
      ]);
    }

    if ($billing_profile = $order->getBillingProfile()) {
      $build['#billing_information'] = $this->profileViewBuilder->view($billing_profile);
    }
    $body = $build;

    return $this->mailHandler->sendMail($to, $subject, $body, $params);
  }

}
