<?php

namespace Drupal\mvi_commerce;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

class MviCommerceServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('commerce_order.order_receipt_mail');
    $definition->setClass('Drupal\mvi_commerce\Mail\OrderReceiptMail');
  }
}
