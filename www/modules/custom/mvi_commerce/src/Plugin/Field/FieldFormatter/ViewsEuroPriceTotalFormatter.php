<?php

namespace Drupal\mvi_commerce\Plugin\Field\FieldFormatter;

use Drupal\commerce\Context;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\PriceCalculatorInterface;
use Drupal\commerce_price\NumberFormatterFactoryInterface;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use CommerceGuys\Intl\Formatter\NumberFormatterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\commerce_store\CurrentStoreInterface;

/**
 * Plugin implementation of the 'views_euro_price_total_formatter' formatter.
 * This FieldFormatter is the same as CustomPriceFormatter.php but then for an orderItem in the shopping cart.
 *
 * @FieldFormatter(
 *   id = "views_euro_pric_total_formatter",
 *   label = @Translation("Views euro price total formatter"),
 *   field_types = {
 *     "commerce_price"
 *   }
 * )
 */
class ViewsEuroPriceTotalFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

    /**
     * The currency storage.
     *
     * @var \Drupal\Core\Entity\EntityStorageInterface
     */
    protected $currencyStorage;

    /**
     * The number formatter.
     *
     * @var \CommerceGuys\Intl\Formatter\NumberFormatterInterface
     */
    protected $numberFormatter;

    protected $priceCalculator;

    protected $currentUser;

    protected $currentStore;

    /**
     * Constructs a new PriceDefaultFormatter object.
     *
     * @param string $plugin_id
     *   The plugin_id for the formatter.
     * @param mixed $plugin_definition
     *   The plugin implementation definition.
     * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
     *   The definition of the field to which the formatter is associated.
     * @param array $settings
     *   The formatter settings.
     * @param string $label
     *   The formatter label display setting.
     * @param string $view_mode
     *   The view mode.
     * @param array $third_party_settings
     *   Any third party settings settings.
     * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
     *   The entity type manager.
     * @param \Drupal\commerce_price\NumberFormatterFactoryInterface $number_formatter_factory
     *   The number formatter factory.
     */
    public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager, NumberFormatterFactoryInterface $number_formatter_factory, CurrentStoreInterface $current_store, AccountInterface $current_user, PriceCalculatorInterface $price_calculator) {
        parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

        $this->currencyStorage = $entity_type_manager->getStorage('commerce_currency');
        $this->numberFormatter = $number_formatter_factory->createInstance();
        $this->numberFormatter->setMaximumFractionDigits(6);
        if ($this->getSetting('strip_trailing_zeroes')) {
            $this->numberFormatter->setMinimumFractionDigits(0);
        }
        if ($this->getSetting('display_currency_code')) {
            $this->numberFormatter->setCurrencyDisplay(NumberFormatterInterface::CURRENCY_DISPLAY_CODE);
        }
        $this->priceCalculator = $price_calculator;
        $this->currentStore = $current_store;
        $this->currentUser = $current_user;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
        return new static(
            $plugin_id,
            $plugin_definition,
            $configuration['field_definition'],
            $configuration['settings'],
            $configuration['label'],
            $configuration['view_mode'],
            $configuration['third_party_settings'],
            $container->get('entity_type.manager'),
            $container->get('commerce_price.number_formatter_factory'),
            $container->get('commerce_store.current_store'),
            $container->get('current_user'),
            $container->get('commerce_order.price_calculator')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode) {
        $elements = [];
        /** @var \Drupal\commerce_price\Plugin\Field\FieldType\PriceItem $item */
        foreach ($items as $delta => $item) {
            $number = null;
            $currency = null;
            $entity = null;
            $originalnumber = $number;
            $entity = $items->getEntity()->getEntityTypeId();
            if($items->getEntity()->getEntityTypeId() == 'commerce_order'){
                /** @var Order $order */
                $order = $items->getEntity();
                $price = $order->getTotalPrice();
                $currency = $this->currencyStorage->load($price->getCurrencyCode());
                $number = $item->number;
                $entity = $order;

            } else {
                $context = new Context($this->currentUser, $this->currentStore->getStore());
                /** @var OrderItem $orderItem */
                $orderItem = $items->getEntity();
                /* @var ProductVariation $entity */
                $entity = $orderItem->getPurchasedEntity();
                $adjustment_types = ['promotion'];
                $result = $this->priceCalculator->calculate($entity, 1, $context, $adjustment_types);
                $calculated_price = $result->getCalculatedPrice();
                $qty = $orderItem->getQuantity();
                $originalnumber = $result->getBasePrice()->getNumber() * $qty;
                $number = $calculated_price->getNumber() * $qty;
                // $product_type = $purchasedEntity->get('type')->getValue()[0]['target_id'];
                $price = $entity->getPrice();
                $currency = $this->currencyStorage->load($price->getCurrencyCode());
            }


            $elements[$delta] = [
                '#theme' => 'euro_price_formatter',
                '#originalnumber' => $originalnumber,
                '#number' => $number,
                '#currency' => $currency,
                '#cache' => [
                    'tags' => $entity->getCacheTags(),
                    'contexts' => Cache::mergeContexts($entity->getCacheContexts(), [
                        'languages:' . LanguageInterface::TYPE_INTERFACE,
                        'country',
                    ]),
                ],
            ];
        }

        return $elements;
    }


}