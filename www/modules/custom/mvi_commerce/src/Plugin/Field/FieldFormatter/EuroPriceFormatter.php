<?php

namespace Drupal\mvi_commerce\Plugin\Field\FieldFormatter;

use CommerceGuys\Intl\Formatter\CurrencyFormatterInterface;
use Drupal\commerce\Context;
use Drupal\commerce_order\Plugin\Field\FieldFormatter\PriceCalculatedFormatter;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\commerce_order\AdjustmentTypeManager;
use Drupal\commerce_order\PriceCalculatorInterface;
use Drupal\commerce_price\NumberFormatterFactoryInterface;
use Drupal\commerce_store\CurrentStoreInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'euro_price_formatter' to show the €sign before the number.
 *
 * @FieldFormatter(
 *   id = "euro_price_formatter",
 *   label = @Translation("Euro price formatter"),
 *   field_types = {
 *     "commerce_price"
 *   }
 * )
 */
class EuroPriceFormatter extends PriceCalculatedFormatter implements ContainerFactoryPluginInterface {

    /**
     * {@inheritdoc}
     */
    protected $adjustmentTypeManager;

    /**
     * {@inheritdoc}
     */
    protected $currentUser;

    /**
     * {@inheritdoc}
     */
    protected $currentStore;

    /**
     * {@inheritdoc}
     */
    protected $priceCalculator;

    public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, CurrencyFormatterInterface $currency_formatter, AdjustmentTypeManager $adjustment_type_manager, CurrentStoreInterface $current_store, AccountInterface $current_user, PriceCalculatorInterface $price_calculator) {
        parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $currency_formatter, $adjustment_type_manager, $current_store, $current_user, $price_calculator);

        $this->adjustmentTypeManager = $adjustment_type_manager;
        $this->currentStore = $current_store;
        $this->currentUser = $current_user;
        $this->priceCalculator = $price_calculator;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
        return new static(
            $plugin_id,
            $plugin_definition,
            $configuration['field_definition'],
            $configuration['settings'],
            $configuration['label'],
            $configuration['view_mode'],
            $configuration['third_party_settings'],
            $container->get('commerce_price.currency_formatter'),
            $container->get('plugin.manager.commerce_adjustment_type'),
            $container->get('commerce_store.current_store'),
            $container->get('current_user'),
            $container->get('commerce_order.price_calculator')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode) {
        $elements = [];
        if (!$items->isEmpty()) {
            $context = new Context($this->currentUser, $this->currentStore->getStore());
            /** @var \Drupal\commerce\PurchasableEntityInterface $purchasable_entity */
            $purchasable_entity = $items->getEntity();
            $adjustment_types = array_filter($this->getSetting('adjustment_types'));
            $result = $this->priceCalculator->calculate($purchasable_entity, 1, $context, $adjustment_types);
            $calculated_price = $result->getCalculatedPrice();
            $originalnumber = $result->getBasePrice()->getNumber();
            $number = $calculated_price->getNumber();
            $currency = $calculated_price->getCurrencyCode();
            $options = $this->getFormattingOptions();

            $elements[0] = [
                '#theme' => 'euro_price_formatter',
                '#currency' => $currency,
                '#originalnumber' => $originalnumber,
                '#number' => $number,
                '#markup' => $this->currencyFormatter->format($number, $currency, $options),
                '#cache' => [
                    'tags' => $purchasable_entity->getCacheTags(),
                    'contexts' => Cache::mergeContexts($purchasable_entity->getCacheContexts(), [
                        'languages:' . LanguageInterface::TYPE_INTERFACE,
                        'country',
                    ]),
                ],
            ];
        }

        return $elements;
    }


}