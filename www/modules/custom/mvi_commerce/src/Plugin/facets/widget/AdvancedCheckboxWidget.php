<?php

namespace Drupal\mvi_commerce\Plugin\facets\widget;

use Drupal\facets\FacetInterface;
use Drupal\facets\Plugin\facets\widget\LinksWidget;

/**
 * The checkbox / radios widget.
 *
 * @FacetsWidget(
 *   id = "advanced_checkbox",
 *   label = @Translation("Advanced list of checkboxes"),
 *   description = @Translation("A configurable widget that shows a list of checkboxes"),
 * )
 */
class AdvancedCheckboxWidget extends LinksWidget {

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet) {
    $build = parent::build($facet);
    $build['#attributes']['class'][] = 'js-facets-checkbox-links';
    $build['#attached']['library'][] = 'facets/drupal.facets.checkbox-widget';
    $build['#attached']['library'][] = 'mvi_commerce/drupal.mvi_daedalus.listpager';
    return $build;
  }

}
