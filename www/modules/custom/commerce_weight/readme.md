When installing the module, make sure to add the Weight quantity text field to the commerce_cart_form view so the quantity can be modified in the cart. When using this field, check the javascript code and do not prepend and append the '+' and '-' buttons as this buttons will be placed by the views field formatter code.

Also use the 'Weight' widget for the order_item type 'weight' in the Add to cart form display. Here you also need to check the javascript code and remove the code that prepends and appends the '+' and '-' as this is included in the fieldformatter.

In the commerce_cart_form view, use the Views euro price weight formatter for the unit price field so the unit is suffixed to the unit price. Example: €18/kg

In the commerce_checkout_order_summary view, use the Weight quantity formatter field.