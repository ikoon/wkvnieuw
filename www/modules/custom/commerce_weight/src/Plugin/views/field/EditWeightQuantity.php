<?php

namespace Drupal\commerce_weight\Plugin\views\field;

use Drupal\commerce_cart\CartManagerInterface;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\UncacheableFieldHandlerTrait;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form element for editing the order item quantity.
 *
 * @ViewsField("commerce_weight_order_item_edit_quantity")
 */
class EditWeightQuantity extends FieldPluginBase {

  use UncacheableFieldHandlerTrait;

  /**
   * The cart manager.
   *
   * @var \Drupal\commerce_cart\CartManagerInterface
   */
  protected $cartManager;

  /**
   * Constructs a new EditQuantity object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_cart\CartManagerInterface $cart_manager
   *   The cart manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CartManagerInterface $cart_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->cartManager = $cart_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('commerce_cart.cart_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function clickSortable() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ResultRow $row, $field = NULL) {
    return '<!--form-item-' . $this->options['id'] . '--' . $row->index . '-->';
  }

  /**
   * Form constructor for the views form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function viewsForm(array &$form, FormStateInterface $form_state) {
    // Make sure we do not accidentally cache this form.
    $form['#cache']['max-age'] = 0;
    // The view is empty, abort.
    if (empty($this->view->result)) {
      unset($form['actions']);
      return;
    }

    $form[$this->options['id']]['#tree'] = TRUE;
    foreach ($this->view->result as $row_index => $row) {
      /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
      $order_item = $this->getEntity($row);
      /* @var ProductVariation $purchasedEntity */
      $purchasedEntity = $order_item->getPurchasedEntity();
      $product_type = $purchasedEntity->bundle();

      if($product_type == 'weight'){
          $step = 50;
          $precision = 0;
          $min = 50;
          $default_value = $order_item->getQuantity() * 1000;
          $suffix = 'gram';
      } else if ($product_type == 'per_person'){
        if($purchasedEntity->hasField('field_minimum_quantity')){
          $min = $purchasedEntity->field_minimum_quantity->value;
        }
        $step = 1;
        $precision = 0;
        $default_value = round($order_item->getQuantity(), $precision);
        $suffix = 'pers';
      } else {
          $step = 1;
          $precision = 0;
          $min = 0;
          $default_value = round($order_item->getQuantity(), $precision);
          $suffix = 'st';
      }

      $form[$this->options['id']][$row_index] = [
        '#type' => 'number',
        '#title' => $this->t('Quantity'),
        '#title_display' => 'invisible',
        '#default_value' => $default_value,
        '#size' => 4,
        '#min' => $min,
        '#max' => 9999,
        '#step' => $step,
        '#prefix' => '<span class="dec qty-button">-</span>',
        '#suffix' => '<span class="unit">' . $suffix . '</span><span class="inc qty-button">+</span>'
      ];
    }
    // Replace the form submit button label.
    $form['actions']['submit']['#value'] = $this->t('Update cart');
  }

  /**
   * Submit handler for the views form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function viewsFormSubmit(array &$form, FormStateInterface $form_state) {
    $quantities = $form_state->getValue($this->options['id'], []);
    foreach ($quantities as $row_index => $quantity) {
      /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
      $order_item = $this->getEntity($this->view->result[$row_index]);
      /* @var ProductVariation $purchasedEntity */
      $purchasedEntity = $order_item->getPurchasedEntity();
      $product_type = $purchasedEntity->bundle();

      if ($order_item->getQuantity() != $quantity) {
          if($product_type == 'weight'){
              $order_item->setQuantity($quantity / 1000);
          } else {
              $order_item->setQuantity($quantity);
          }
          $order = $order_item->getOrder();
          $this->cartManager->updateOrderItem($order, $order_item, FALSE);
          // Tells commerce_cart_order_item_views_form_submit() to save the order.
          $form_state->set('quantity_updated', TRUE);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing.
  }

}
