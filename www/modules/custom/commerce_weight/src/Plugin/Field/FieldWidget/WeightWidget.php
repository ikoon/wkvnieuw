<?php

namespace Drupal\commerce_weight\Plugin\Field\FieldWidget;

use Drupal\commerce_order\Plugin\Field\FieldWidget\QuantityWidget;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'commerce_weight' widget.
 *
 * @FieldWidget(
 *   id = "commerce_weight",
 *   label = @Translation("Weight"),
 *   field_types = {
 *     "decimal",
 *   }
 * )
 */
class WeightWidget extends QuantityWidget {

    /**
     * {@inheritdoc}
     */
    public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
        $element = parent::formElement($items, $delta, $element, $form, $form_state);
        $element['value']['#step'] = $this->getSetting('step');
        $element['value']['#default_value'] = 100;
        $element['value']['#step'] = 50;
        $element['value']['#min'] = 50;
        $element['value']['#element_validate'] = [
            [$this, 'validate'],
        ];

        if(array_key_exists('quantity', $form_state->getUserInput())){
            $value = $form_state->getUserInput()['quantity'][0]['value'];
            $form_state->getUserInput()['quantity'][0]['value'] = $value / 1000;
        }

        return $element;
    }

    /**
     * Validate the weight quantity field
     */
    public static function validate($element, FormStateInterface $form_state) {
        $value = $element['#value'];

        $form_state->setValueForElement($element, $value/1000);
    }


    /**
     * {@inheritdoc}
     */
    public static function isApplicable(FieldDefinitionInterface $field_definition) {
        $entity_type = $field_definition->getTargetEntityTypeId();
        $field_name = $field_definition->getName();
        return $entity_type == 'commerce_order_item' && $field_name == 'quantity';
    }

}
