<?php

/**
 * @file
 * Contains ajax_add_to_cart.module.
 */

use Drupal\ajax_add_to_cart\Helper\AjaxCartHelper;
use Drupal\commerce_cart\Form\AddToCartFormInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_theme().
 */
function ajax_add_to_cart_theme($existing, $type, $theme, $path) {
  return [
    'add_to_cart' => [
      'variables' => [
        'quantity' => 1,
        'product_entity' => NULL,
        'price' => null
      ],
    ],
  ];
}

/**
 * Implements hook_help().
 */
function ajax_add_to_cart_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the ajax_add_to_cart module.
    case 'help.page.ajax_add_to_cart':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Add ajax add to cart functionality.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Define changes for standard forms.
 */
function ajax_add_to_cart_form_alter(&$form, &$form_state, $form_id) {
  // Check if the form builder implements the AddToCartFormInterface.
  if ($form_state->getBuildInfo()['callback_object'] instanceof AddToCartFormInterface) {
    // Using getInstanace method to create Object.
    $object = AjaxCartHelper::getInstance();
    $object->ajaxAddToCartAjaxForm($form_id, $form);
  }
}

/**
 * Define a validation for forms.
 */
function ajax_add_to_cart_ajax_validate(&$form, $form_state) {
  $response = new AjaxResponse();
  $form_id = $form_state->getUserInput()['form_id'];
  $variationId = $form_state->getValue('purchased_entity')[0]['variation'];
  $productVariation = \Drupal\commerce_product\Entity\ProductVariation::load($variationId);
  $product = $productVariation->getProduct();
  $quantity = $form_state->getValue('quantity')[0]['value'];

  $price = $productVariation->price->view('default');

  /** @var \Drupal\Core\Form\FormStateInterface $form_state */
  if ($form_state->hasAnyErrors()) {
    // If form is having errors, rebuild form.
    $errors = $form_state->getErrors();
    $form['#errors'] = $errors;

    $response->addCommand(new ReplaceCommand('#commerce-product-' . $form_id, $form));
  }
  else {
    // Using getInstanace method to create Object.
    $object = AjaxCartHelper::getInstance();
    // If validated successfully submit form.
    $object->ajaxAddToCartAjaxResponse($form_id, $response, $product, $quantity, $price);
  }
  return $response;
}

/**
 * Implements hook_preprocess_block().
 */
function ajax_add_to_cart_preprocess_block(&$vars) {
  if ($vars['base_plugin_id'] == 'commerce_cart') {
    // Add a class to the cart block so that we can replace it using Ajax.
    $vars['attributes']['class'][] = 'block-commerce-cart';
  }
}

/**
 * Fixes «Ajax Paging in Views breaks Add to Cart form»  https://www.drupal.org/project/commerce/issues/2916671
 * Implements form_commerce_order_item_add_to_cart_form_alter
 */
function ajax_add_to_cart_form_commerce_order_item_add_to_cart_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $request = \Drupal::request();
  $views_ajax = preg_match( '{views(\.|/)ajax}', $request->getRequestUri());
  $route = $request->attributes->get('_route') == 'views.ajax';
  if ($views_ajax || $route) {
    $path = $request->attributes->get('_route_object')->getPath();
    $page = $request->query->get('page');
    $action = $page ? "$path?page=$page" : $path;
    $form['#action'] = $action;
  }
}

/**
 * Implements hook_views_pre_view().
 */
function ajax_add_to_cart_views_pre_view(\Drupal\views\ViewExecutable $view, $display_id, array &$args)
{
  if (!preg_match('/^commerce_product_/', $view->storage->get('base_table'))) {
    return;
  }

  $object = $view->getRequest()->attributes->get('_route_object');
  if ($object && $view->getRequest()->attributes->get('_route') == 'views.ajax') {
    $path = $view->hasUrl($args, $display_id) ? $view->getUrl()->toString() : trim(explode('?', $view->getPath())[0], '/');
    $object->setPath("/$path");
    $view->getRequest()->attributes->set('_route_object', $object);
  }
}
