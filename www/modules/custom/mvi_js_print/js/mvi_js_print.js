(function ($, window) {
  "use strict";
  /**
   * Trigger window.print on every element with the "data-js-print" attribute
   */
  Drupal.behaviors.mviJsPrint = {
    attach: function (context, settings) {
      $(context).find('[data-js-print]').not('[data-js-print-initted]').each(function () {
        $(this).attr('data-js-print-initted', '').on('click', function (e) {
          e.preventDefault();
          window.print();
        });
      });
    }
  };


})(jQuery, window);
