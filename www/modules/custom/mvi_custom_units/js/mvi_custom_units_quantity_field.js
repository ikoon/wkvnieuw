(function ($, window) {
  "use strict";
  /**
   * MVI Commerce Custom Units - logic for quantity field
   */
  Drupal.behaviors.mviCustomUnitsQuantityField = {
    attach: function (context, settings) {
      var me = this;
      $(context).find('[data-custom-unit-input]').not('[data-custom-unit-initted]').each(function () {
        var $wrapper = $(this).closest('.js-form-wrapper');
        // for the cart view
        if (!$wrapper[0]) {
          $wrapper = $(this).closest('.views-field-edit-quantity');
        }
        $(this).attr('data-custom-unit-initted', '1');
        me.init($wrapper);
      });
    },

    init: function ($wrapper) {
      var me = this;
      var currentQuantity = this.getCurrentQuantity($wrapper);
      var step = this.getStep($wrapper);
      if (currentQuantity % step !== 0) {
        this.setCurrentQuantity($wrapper, step);
      }
      $wrapper.find('[data-custom-unit-decr], [data-custom-unit-incr]').on('click', function (e) {
        e.preventDefault();
        me.handleDecrIncr($wrapper, $(this));
      });
      $wrapper.find('[data-custom-unit-input]').attr('readonly', '1');
    },

    handleDecrIncr: function ($wrapper, $btn) {
      var isIncr = typeof($btn.attr('data-custom-unit-incr')) !== 'undefined';
      var newQuantity = this.getCurrentQuantity($wrapper) + ((this.getStep($wrapper)) * (isIncr ? 1 : -1));
      this.setCurrentQuantity($wrapper, newQuantity);
    },

    getStep: function ($wrapper) {
      return parseFloat($wrapper.find('[data-custom-unit-input]').attr('data-custom-unit-step'));
    },

    getVolume: function ($wrapper) {
      return parseFloat($wrapper.find('[data-custom-unit-input]').attr('data-custom-unit-volume'));
    },

    getCurrentQuantity: function ($wrapper) {
      return parseFloat($wrapper.find('[data-custom-unit-input]').val());
    },

    getLabelEl: function ($wrapper) {
      return $wrapper.find('[data-custom-unit-lbl]');
    },

    setCurrentQuantity: function ($wrapper, val) {
      if (val > 0) {
        var step = this.getStep($wrapper);
        val = val - (val % step);
        if (val <= 0) {
          val = step;
        }
        $wrapper.find('[data-custom-unit-input]').val(val);
        // var $lbl = this.getLabelEl($wrapper);
        // var volume = this.getVolume($wrapper);
        // $lbl.text($lbl.attr('data-custom-unit-lbl').replace('***AMOUNT***', val * volume));
      }
    }
  };


})(jQuery, window);
