# MVI Commerce Custom Units README

## Installation
- Enable this module, you will notice the following:
	- 1 new Taxonomy Vocabulary called "Volume units", create as many of these as you want. Create at least one, which will be the default ("Piece" for example)
	- 1 new Content types:
		- Custom unit : DO NOT MANUALLY CREATE CONTENT OF THIS CONTENT TYPE, it is used to create a single ref field, which is added to the product variation (and can be added to other product variations as well)
			- Tip: after creating one Taxonomy Term from type "Volume units", set this as the default value for the field "Volume Unit" at /admin/structure/types/manage/custom_unit/fields/node.custom_unit.field_volume_unit to improve usability.
	- 1 new field "One product contains" that is added to the default Product Variation Type. Add this to other Product Variation Types where needed. Check out the config at /admin/commerce/config/product-variation-types/default/edit for more info.
		- When adding a new product, or editing an existing product, check out this new field, the description with prefix / suffix should be self-explanatory.

## Example
A Product with name 'Soda' has a content of 0.5L of Soda. These are sold per 6, a customer can only by 6, 12, 18, 24,... of these products:

- Create a new taxonomy term of type "Volume units" named "L".
- Create a new product called 'Soda'
	- Title: "Soda"
	- One product contains: 
		- Volume: 0.5
		- Unit: "L"
		- Quantity step: Can be ordered per "6" products
	- Fill in the rest of the product's fields and save
- Put a couple of the 'Soda' products in the shopping cart and go to checkout, you will notice the quantity field is read-only and the "+" "-" button increase/decrease by 6. While the total volume label updates with the correct amount of liters (should be 3L or 6L or 9L or ...)
- When this order is place, notice the same behavior in the admin panel, the quantity can only be increased by 6 at the time
